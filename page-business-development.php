
<div class="business_Wrapper">
  <div class="page_Header">
    <h1>Business Development Home</h1>
  </div>
  
<div class="page_Content bis_Wrapper">
  <!-- Carousel -->
  <?php if (Kirki::get_option( 'bis_carousel_toggle' )) : ?>
    <!-- Wrapper -->
    <div class="titleBox_Wrapper">
      <div class="titleBox_Container">
        <?php 
          //Consts
          $iVal = array('one', 'two', 'three', 'four', 'five', 'six');
          //Images
          for ($i= 0; $i < count($iVal); $i++) {
            //constanct from customizer
            $const =  'bis_carousel_' . $iVal[$i] . '_';
            $toggle = Kirki::get_option( $const . 'toggle' );
            $title = Kirki::get_option( $const . 'title' );

            if ($toggle) {
              echo('<div class="box">');
                echo('<div class="title">' . $title . '</div>');
              echo('</div>');
            }
          }
        ?>
      </div>
    </div>
    
    <div class="bis_Carousel_Wrapper">
      <div class="carousel">
        <?php
          //Images
          for ($j= 0; $j < count($iVal); $j++) {
            //constanct from customizer
            $const =  'bis_carousel_' . $iVal[$j] . '_';
            $toggle = Kirki::get_option( $const . 'toggle' );
            $textPos = 'text_Wrapper ' . Kirki::get_option( $const . 'textPos' );
            $subTitle = Kirki::get_option( $const . 'subTitle' );
            $subTitleColor = 'color:' . Kirki::get_option( $const . 'subTitleColor' ) . ";";
            $subTitleBg = 'background:' . Kirki::get_option( $const . 'subTitleBg' ) . ";";
            $desc = Kirki::get_option( $const . 'desc' );
            $descColor = 'color:' . Kirki::get_option( $const . 'descColor' ) . ";";
            $descBg = 'background:' . Kirki::get_option( $const . 'descBg' ) . ";";
            $img = "background-image: url(" . Kirki::get_option( $const . 'img' ) .")";

            if ($toggle) {
              echo('<div class="carousel_img"  style=" ' . $img . '" >' );
                echo('<div class="text_Wrapper">');
                  echo('<div class="title' . $textPos . '"><h3 style = "' . $subTitleColor . ' ' . $subTitleBg.  '" >' . $subTitle . '</h3></div>');
                  echo('<div class="desc"><p style = "' . $descColor . ' ' . $descBg . '">' . $desc . '</p></div>');
                echo('</div>');
              echo('</div>');
            }
          }
        ?>
      </div>      
    </div>

  <?php endif; ?>
  

  
<div id="bisALMFeatured">
  <div id="ajax-load-more" class="ajax-load-more-wrap loading">
    <ul class="alm-listing alm-ajax alm-paging-wrap  " data-paging="true" data-paging-controls="true" data-paging-show-at-most="7" data-paging-classes="bis-featured-stories" data-repeater="template_2" data-post-type="featured-story" data-post-format="" data-category="" data-category-not-in="" data-tag="" data-tag-not-in="" data-taxonomy="" data-taxonomy-terms="" data-taxonomy-operator="" data-meta-key="carousel_position" data-meta-value="" data-meta-compare="" data-meta-relation="" data-year="" data-month="" data-day="" data-author="" data-post-in="" data-exclude="" data-search="" data-custom-args="" data-custom-id="bisHomePageVids" data-post-status="" data-order="ASC" data-orderby="meta_value_num" data-offset="0" data-posts-per-page="1" data-lang="" data-scroll="false" data-scroll-distance="150" data-max-pages="5" data-pause="true" data-button-label="Older Posts" data-button-class="" data-destroy-after=""  data-transition="slide"></ul>
  </div>
</div>

<!--   <?php get_template_part('templates/content', 'page'); ?> -->
  
</div>