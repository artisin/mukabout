<!-- Carousel -->
<?php get_template_part('lib/components/home_carousel'); ?>
  
<div id="homeALMFeatured">
  
  <div class="topSpacer"></div>
  <video src="" class="video-js vjs-default-skin vjs-artisin-skin" controls preload="auto" width="100%" ytcontrols="true" height="300" data-setup='{ "techOrder": ["youtube"], "src": "https://www.youtube.com/watch?v=viz1uIQLfxs" }'></video>
  
  <div class="story_Wrapper">
    <div class="storyHead">
      <div class="header">
        <h2>Citizens Bank: ‘Mukwonago Aggressively Growing’</h2>
      </div>
    </div>
    <div class="storyDetails">
      <p>Citizens Bank’s Charlie Miller describes village leadership as much more aggressive in pursuing development than years past. Developers are finding the village willing to listen and to take efficient action toward a mutually beneficial outcome.</p>
    </div>
  </div>

</div>


  <div class="list_pannel_Wrapper">
    <?php if ( is_active_sidebar( 'home_left' ) ) : ?> 
      <?php { dynamic_sidebar( 'home_left' ); } ?>
    <?php endif; ?>
    <?php if ( is_active_sidebar( 'home_middle' ) ) : ?> 
      <?php { dynamic_sidebar( 'home_middle' ); } ?>
    <?php endif; ?>
    <?php if ( is_active_sidebar( 'home_right' ) ) : ?> 
      <?php { dynamic_sidebar( 'home_right' ); } ?>
    <?php endif; ?>
  </div>
