<?php
/**
 * Kirki Advanced Customizer
 * This is a sample configuration file to demonstrate all fields & capabilities.
 * @package Kirki
 */

// Early exit if Kirki is not installed
if ( ! class_exists( 'Kirki' ) ) {
  return;
}

/**
 * Helpers
 */
function convert_number_to_words($number) {
    
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
    
    if (!is_numeric($number)) {
        return false;
    }
    
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
    
    $string = $fraction = null;
    
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
    
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
    
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
    
    return $string;
}

//Business
Kirki::add_panel( 'bis', array(
    'priority'    => 10,
    'title'       => 'Business Development',
    'description' => 'Customize the assests for the business development page.',
));
//Carousel
Kirki::add_section( 'carousel_controls', array(
  'priority'    => 10,
  'panel'       => 'bis',
  'title'       => 'Carousel',
  'description' => 'Customize the assests for the business development page Carousel.',
));


// Toggle
Kirki::add_field( 'bis_carousel_toggle', array(
  'type'        => 'toggle',
  'settings'    => 'bis_carousel_toggle',
  'label'       => 'Enable Carousel',
  'description' => 'This is the switch will turn on or off the entire carousel',
  'section'     => 'carousel_controls',
  'default'     => 1,
  'priority'    => 10,
));

/**
 * Carousel One 
 */
for ($i=1; $i <= 6; $i++) {
  $num = convert_number_to_words($i);
  $const =  'bis_carousel_' . $num . '_';
  $toggle = $const . 'toggle';
  $title = $const . 'title';
  $textPos = $const . 'textPos';
  $subTitle = $const . 'subTitle';
  $subTitleColor = $const . 'subTitleColor';
  $subTitleBg = $const . 'subTitleBg';
  $desc = $const . 'desc';
  $descColor = $const . 'descColor';
  $descBg = $const . 'descBg';
  $img = $const . 'img';
  $defaultToggle = 1;
  if ($i > 3) {
    $defaultToggle = 0;
  }
  //Toggle
  Kirki::add_field( $toggle, array(
    'type'        => 'toggle',
    'settings'    => $toggle,
    'label'       => 'Enable Carousel ' . $num,
    'description' => 'This is the switch will turn on or off carousel ' . $num,
    'section'     => 'carousel_controls',
    'default'     => $defaultToggle,
    'priority'    => 10,
  ));
  //Title
  Kirki::add_field( $title, array(
      'type'        => 'text',
      'settings'    => $title,
      'label'       => 'Title for carousel image ' . $num,
      'section'     => 'carousel_controls',
      'default'     => 'Title',
      'priority'    => 10,
  ));
  Kirki::add_field( $textPos, array(
      'type'        => 'radio',
      'settings'     => $textPos,
      'label'       => 'Text position for carousel image ' . $num,
      'section'     => 'carousel_controls',
      'default'     => 'left',
      'priority'    => 10,
      'choices'     => array(
          'left' => __( 'Left', 'Left' ),
          'right' => __( 'Right', 'Right' ),
      ),
  ) );
  Kirki::add_field( $subTitle, array(
      'type'        => 'text',
      'settings'    => $subTitle,
      'label'       => 'Sub-Title for carousel image ' . $num,
      'section'     => 'carousel_controls',
      'default'     => 'Sub-Title',
      'priority'    => 10,
  ));
  Kirki::add_field( $subTitleColor, array(
      'type'        => 'color-alpha',
      'settings'     => $subTitleColor,
      'label'       => 'Sub-Title Color for carousel image ' . $num,
      'section'     => 'carousel_controls',
      'default'     => 'rgba(236, 240, 241, 0.9)',
      'priority'    => 10,
  ) );
  Kirki::add_field( $subTitleBg, array(
      'type'        => 'color-alpha',
      'settings'     => $subTitleBg,
      'label'       => 'Sub-Title background for carousel image ' . $num,
      'section'     => 'carousel_controls',
      'default'     => 'rgba(0,0,0,.7)',
      'priority'    => 10,
  ) );

  Kirki::add_field( $desc, array(
      'type'        => 'textarea',
      'settings'    => $desc,
      'label'       => 'Description for carousel image '  . $num,
      'section'     => 'carousel_controls',
      'default'     => 'This is some default text',
      'priority'    => 10,
  ) );
  Kirki::add_field( $descColor, array(
      'type'        => 'color-alpha',
      'settings'     => $descColor,
      'label'       => 'Description Color for carousel image ' . $num,
      'section'     => 'carousel_controls',
      'default'     => 'rgba(0,0,0,.7)',
      'priority'    => 10,
  ) );
  Kirki::add_field( $descBg, array(
      'type'        => 'color-alpha',
      'settings'     => $descBg,
      'label'       => 'Description background for carousel image ' . $num,
      'section'     => 'carousel_controls',
      'default'     => 'rgba(236, 240, 241, 0.9)',
      'priority'    => 10,
  ) );
  Kirki::add_field( $img, array(
      'type'        => 'image',
      'settings'    => $img,
      'label'       => 'Pick a image for carousel ' . $num,
      'description' => 'Best Formate is 965 x 400',
      'section'     => 'carousel_controls',
      'default'     => '',
      'priority'    => 10,
  ));
}

/**
 * Create panels using the Kirki API
 */
// Kirki::add_section( 'carousel_controls', array(
//   'priority'    => 10,
//   'panel' => 'bis',
//   'title'       => __( 'Boolean Controls', 'kirki' ),
//   'description' => __( 'This panel contains controls that return true/false Controls', 'kirki' ),
// ) );



/**
 * Add the background field
 */
Kirki::add_field( 'kirki_demo', array(
  'type'        => 'background',
  'settings'    => 'background_demo',
  'label'       => __( 'This is the label', 'kirki' ),
  'description' => __( 'This is the control description', 'kirki' ),
  'help'        => __( 'This is some extra help. You can use this to add some additional instructions for users. The main description should go in the "description" of the field, this is only to be used for help tips.', 'kirki' ),
  'section'     => 'background_section',
  'default'     => array(
    'color'    => '#ffffff',
    'image'    => '',
    'repeat'   => 'no-repeat',
    'size'     => 'cover',
    'attach'   => 'fixed',
    'position' => 'left-top',
    'opacity'  => 90,
  ),
  'priority'    => 10,
  'output'      => '.hentry',
) );
/**
 * Configuration sample for the Kirki Customizer.
 */
function kirki_demo_configuration_sample() {

  $args = array(
    'description'  => __( 'This is the theme description. You can edit it in the Kirki configuration and add whatever you want here.', 'kirki' ),
    // 'disable_output' => true,
  );

  return $args;

}

add_filter( 'kirki/config', 'kirki_demo_configuration_sample' );
