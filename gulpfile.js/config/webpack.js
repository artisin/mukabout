var path            = require('path'),
    paths           = require('./'),
    webpack         = require('webpack');

module.exports = function(env) {
  var jsSrc = path.resolve(paths.sourceAssets + '/js'),
      jsDest = paths.publicAssets + '/js',
      publicPath = 'assets/js/';

  var webpackConfig = {
    context: jsSrc,
    entry: {
      main: "./main.js",
      vendor: "./vendor.js"
    },

    output: {
      path: jsDest,
      filename: env === 'production' ? '[name].js' : '[name].js',
      chunkFilename: env === 'production' ? '[name].js' : '[name].js',
      publicPath: publicPath
    },
    target: 'web',
    plugins: [],

    resolve: {
      extensions: ['', '.js'],
      modulesDirectories: ['node_modules']
    },

    module: {
      loaders: [
        {
          test: /\.js?$/,
          loader: 'babel',
          exclude: /(node_modules|bower_components)/,
          query: {
            loose: 'all'
          }
        },
        {
          test: /\.txt$/,
          loader: 'raw-loader',
          exclude: /(node_modules|bower_components)/
        }
      ]
    }
  };

  if(env !== 'test') {
    // Factor out common dependencies into a shared.js
    webpackConfig.plugins.push(
      new webpack.optimize.CommonsChunkPlugin({
        name: 'common',
        filename: env === 'production' ? '[name].js' : '[name].js',
        chunks: ['vendor']
      })
    );
  }

  if(env === 'development') {
    webpackConfig.devtool = 'source-map';
    webpack.debug = true;
  }

  if(env === 'production') {
    webpackConfig.plugins.push(
      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),
      new webpack.optimize.UglifyJsPlugin(),
      new webpack.NoErrorsPlugin(),
      new webpack.optimize.DedupePlugin(),
      new webpack.ProvidePlugin({
          $: "jquery",
          jQuery: "jquery",
          jquery: "jquery",
          "window.jQuery": "jquery"
      })
    );
  }

  return webpackConfig;
};
