var config = require('./');

module.exports = {
  src: config.sourceAssets + "/styles/**/*.{sass,scss}",
  dest: config.sourceAssets + '/styles/_compiled',
};