var config = require('./');

module.exports = {
  src: config.sourceAssets + '/js/imports/**/*',
  dest: config.publicAssets + '/js'
};
