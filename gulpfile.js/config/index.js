var config = {};

config.sourceDirectory = "./assets";
config.publicDirectory = "./dist";
config.publicAssets    = config.publicDirectory;
config.sourceAssets    = config.sourceDirectory;
config.publicTemp    = config.publicDirectory + "/.temp";

module.exports = config;
