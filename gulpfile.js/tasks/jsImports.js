var gulp        = require('gulp'),
    browserSync = require('browser-sync'),
    changed     = require('gulp-changed'),
    config      = require('../config/jsImports'),
    uglify      = require('gulp-uglifyjs'),
    gulpif      = require('gulp-if'),
    argv        = require('yargs').argv,
    devel       = argv._[0] === undefined;

gulp.task('jsImports', function() {
  return gulp.src(config.src)
    .pipe(gulpif(devel, changed(config.dest)))
    .pipe(gulpif(!devel, changed(uglify())))
    .pipe(gulp.dest(config.dest))
    .pipe(browserSync.reload({stream:true}));
});
