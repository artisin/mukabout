var gulp         = require('gulp');
var gulpSequence = require('gulp-sequence');


gulp.task('development', function(cb) {
  gulpSequence(
    'clean', 
    ['fonts', 'jsImports', 'images'], 
    ['postCss', 'webpack:development'],
    ['watch', 'browserSync'], 
  cb);
});
