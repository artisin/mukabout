var gulp      = require('gulp'),
    watch     = require('gulp-watch'),
    sass      = require('../config/sass'),
    images    = require('../config/images'),
    stylus    = require('../config/stylus'),
    jsImports = require('../config/jsImports'),
    fonts     = require('../config/fonts');

//Watch said files
gulp.task('watch', ['browserSync'], function() {
  watch(images.src, function() { gulp.start('images'); });
  watch(stylus.src, function() { gulp.start('postCss'); });
  watch(jsImports.src, function() { gulp.start('jsImports'); });
  watch(sass.src, function() { gulp.start('sass'); });
  watch(fonts.src, function() { gulp.start('fonts'); });
});
