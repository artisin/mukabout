var browserSync = require('browser-sync'),
    gulp        = require('gulp');

gulp.task('browserSync', function() {
  return browserSync.init({
      files: ['{lib,templates}/**/*.php', '*.php'],
      proxy: "http://vccw.dev/",
      snippetOptions: {
        whitelist: ['/wp-admin/admin-ajax.php'],
        blacklist: ['/wp-admin/**']
      }
  });
});
