TODO
========
--darken email field blackish 
--slick === vid ;; height
-vid home 100
-slick arrow shodow hover
-top boarder of list pannels -gray ?
-footer vh100
- link to act not read more
#####Setup
+ [x] Vagrant
    * [x] Custom box
        - [x] CentOs
            + [x] Git
            + [x] jq
        - [x] Wordpress Latest
            + [x] WP-CLI
            + [x] Tools i18n
            + [x] php sniffer
        - [x] php
            + [x] phpUnit
            + [x] composer
        - [x] MySql
        - [x] Appache
        - [x] node
+ [x] Gulp
    * [x] Sass
    * [x] Styl
    * [x] Js
        - [x] Bable
        - [x] webkit
    * [x] Templating
    * [x] Clean up, ect
    * [x] Prod
    * [x] Build

#####Build
+ [ ] Header
    * [x] Basic Layout
    * [ ] Styles
        - [x] Desktop
            + [x] Hover logic
                * Prbly change scheme color to bone white/alpha bg
            + [x] Js login for inner pages
            + [x] Reconfig logic to handle x links
        - [ ] Mobile Logic
            + Need to get nav structor first
+ HomePage
    * [x] Basic Layout
    * [x] Styles
    * [ ] Featured Stories / intro
        - Postponed?
        - [ ] Logic
            + [ ] Custom post type
        - [x] Styles
    * [x] New Feed widgets
        - [x] Logic
        - [x] Styles
+ [ ] Footer
    * [x] Basic Layout
    * [ ] Widgets
        - [ ] Social Media
            + [ ] Styles
            + [x] Logic
        - [ ] Social Media
            + [ ] Styles
            + [x] Logic
        - [ ] Contact
            + [ ] Styles
            + [x] Logic
    * [ ] Google Maps
        - [ ] Styles
        - [x] Logic
    * [] Bottom Nav
        - [ ] Styles
        - [x] Logic
+ [ ] SubPages
    * [x] Custom side-bar contact logic
    * [ ] Custom buttons for styles
    * [ ] Business
        - [ ] Home page
            + [ ] Featured Stories (vids)
                * [ ] Pagination
                    - [x] Basic Logic
                    - [x] Styles
                    - [ ] Ajax logic
                * [x] Custom Post Time
                    - [x] Vid 
                    - [x] Carousle Pos
                    - [x] Add
                    - [x] Remove
            + [x] Picture carousle
            + [ ] Text desc, anything???
        - [x] Sub pages
    * [ ] Community
        - Postponed
    * [ ] Goverment
        - Postponed
+ [ ] Mobile Logic
    * [ ] Header
    * [ ] Footer
    * [ ] Subpages
+ [ ] Set Plugin Activation
    * [x] Soil
    * [x] Ajax Load More
        - [x] Custom logic
        - [x] Async sequence flow-control logic
        - [X] Ajax Loader Paging
        - [x] Ajax Loader SEO
        - [ ] Youtube pagnation
    * [x] SCP Types
    * [X] Roots Wrapper 
    * [x] Twitter Feed
        - [x] Twitter API Key congif
+ [x] Google map api key
+ [ ] Customizer
    * [x] Carousle Gen
        - [x] Slick logic
        - [x] Js logic
        - [x] Styles
        - [x] Template
    * [x] Home Carousle
        - Other logic I will come later after I have a better feel
    * [ ] Bis Carousle
    * [ ] Com Carousle

#####Finilize
+ [ ] Clean
    * [ ] php
    * [ ] styles
    * [ ] js
+ [ ] Documentation

