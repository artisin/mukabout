<?php
/**
 * Functions used to implement options
 *
 */
/**
 * Enqueue Google Fonts Example
 */
function mukabout_fonts() {
  // Font options
  $fonts = array(
    get_theme_mod( 'primary-font', customizer_library_get_default( 'primary-font' ) ),
    get_theme_mod( 'secondary-font', customizer_library_get_default( 'secondary-font' ) )
  );
  $font_uri = customizer_library_get_google_font_uri( $fonts );
  // Load Google Fonts
  wp_enqueue_style( 'mukabout_fonts', $font_uri, array(), null, 'screen' );
}
add_action( 'wp_enqueue_scripts', 'mukabout_fonts' );

/**
 * Custom Customizer Style
 */
function mukabout_customizer_style() {

  wp_enqueue_style( 'mukabout-customizer-style', get_template_directory_uri() . '/customizer/customizer.css', array(), '', 'all' );

}
add_action( 'customize_controls_enqueue_scripts', 'mukabout_customizer_style' );