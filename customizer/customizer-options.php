<?php
/**
 * Defines customizer options
 *
 * @package Customizer_Library
 */

function customizer_library_mukabout_options() {

  // Theme defaults
  $primary_color = '#e8554e';
  $secondary_color = '#666';
  $top_header = '#2c3135';
  $primary_green = '#1fa67a';
  $primary_green_hover = '#198562';
  $donation_bg = '#f1c40f';
  $donation_border = '#e2b709';
  $home_top_widgets_bg = '#34495e';
  $home_top_widgets_hover = '#a2da08';
  $social_icon = '#aaaaaa';
  $white = '#ffffff';
  $top_nav_link_color = '#888888';

  // Categories
  $categories = get_categories();
  $cats = array();
  $i = 0;
  foreach($categories as $category){
    if($i==0){
      $default = $category->slug;
      $i++;
    }
    $cats[$category->slug] = $category->name;
  }

    // Animations
    $animate = array(
        'none' => __('none', 'mukabout'),
        'bounce' => __('bounce', 'mukabout'),
        'flash' => __('flash', 'mukabout'),
        'pulse' => __('pulse', 'mukabout'),
        'shake' => __('shake', 'mukabout'),
        'wobble' => __('wobble', 'mukabout'),
        'bounceIn' => __('bounceIn', 'mukabout'),
        'bounceInDown' => __('bounceInDown', 'mukabout'),
        'bounceInLeft' => __('bounceInLeft', 'mukabout'),
        'bounceInRight' => __('bounceInRight', 'mukabout'),
        'bounceInUp' => __('bounceInUp', 'mukabout'),
        'fadeIn' => __('fadeIn', 'mukabout'),
        'fadeInDown' => __('fadeInDown', 'mukabout'),
        'fadeInDownBig' => __('fadeInDownBig', 'mukabout'),
        'fadeInLeft' => __('fadeInLeft', 'mukabout'),
        'fadeInLeftBig' => __('fadeInLeftBig', 'mukabout'),
        'fadeInRight' => __('fadeInRight', 'mukabout'),
        'fadeInRightBig' => __('fadeInRightBig', 'mukabout'),
        'fadeInUp' => __('fadeInUp', 'mukabout'),
        'fadeInUpBig' => __('fadeInUpBig', 'mukabout'),
        'lightSpeedIn' => __('lightSpeedIn', 'mukabout'),
        'lightSpeedOut' => __('lightSpeedOut', 'mukabout'),
        'rotateIn' => __('rotateIn', 'mukabout'),
        'rotateInDownLeft' => __('rotateInDownLeft', 'mukabout'),
        'rotateInDownRight' => __('rotateInDownRight', 'mukabout'),
        'rotateInUpLeft' => __('rotateInUpLeft', 'mukabout'),
        'rotateInUpRight' => __('rotateInUpRight', 'mukabout'),
        'zoomIn' => __('zoomIn', 'mukabout'),
        'zoomInDown' => __('zoomInDown', 'mukabout'),
        'zoomInLeft' => __('zoomInLeft', 'mukabout'),
        'zoomInRight' => __('zoomInRight', 'mukabout'),
        'zoomInUp' => __('zoomInUp', 'mukabout')
    );

  // Stores all the controls that will be added
  $options = array();

  // Stores all the sections to be added
  $sections = array();

  // Image path defaults=
  // $imagepath =  get_template_directory_uri() . '/img/';

  // Adds the sections to the $options array
  $options['sections'] = $sections;

  // Header
  $section = 'header';

  $sections[] = array(
    'id'      => $section,
    'title'     => __( 'Header', 'mukabout' ),
    'priority'    => '30',
    'description'   => __( 'Header area options', 'mukabout' ),
    'panel'     => 'theme_options',
  );

  $options['header_logo'] = array(
    'id'    => 'header_logo',
    'label'     => __( 'Logo', 'mukabout' ),
    'section'   => $section,
    'type'      => 'image',
    'default'   => '',
  );

  $options['header_top_bg_color'] = array(
    'id'    => 'header_top_bg_color',
    'label'     => __( 'Top header background color', 'mukabout' ),
    'section' => $section,
    'type'      => 'color',
    'default' => $top_header,
    'transport' => 'postMessage',
  );

  $options['header_top_nav_link_color'] = array(
    'id'    => 'header_top_nav_link_color',
    'label'     => __( 'Top header Navigation Link Color', 'mukabout' ),
    'section' => $section,
    'type'      => 'color',
    'default' => $top_nav_link_color,
    'transport' => 'postMessage',
  );

  $options['header_top_nav_link_color_hover'] = array(
    'id'    => 'header_top_nav_link_color_hover',
    'label'     => __( 'Top header Navigation Link Hover Color', 'mukabout' ),
    'section' => $section,
    'type'      => 'color',
    'default' => $white,
  );

  $options['header_bottom_bg_color'] = array(
    'id'    => 'header_bottom_bg_color',
    'label'     => __( 'Bottom header background color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $primary_green,
    'transport' => 'postMessage',
  );

  $options['header_bottom_nav_link_color'] = array(
    'id'    => 'header_bottom_nav_link_color',
    'label'     => __( 'Bottom Header Navigation Link Color', 'mukabout' ),
    'section' => $section,
    'type'      => 'color',
    'default' => $white,
    'transport' => 'postMessage',
  );

  $options['header_bottom_nav_dropdown_indicator'] = array(
    'id' => 'header_bottom_nav_dropdown_indicator',
    'label'     => __( 'Check to remove the dropdown indicator circle', 'mukabout' ),
    'section'   => $section,
    'type'      => 'checkbox',
    'default'   => 0,
  );

  $options['donation_button_show'] = array(
    'id' => 'donation_button_show',
    'label'     => __( 'Display donation button', 'mukabout' ),
    'section'   => $section,
    'type'      => 'checkbox',
    'default'   => 0,
  );

  $options['donate_button_text'] = array(
    'id'    => 'donate_button_text',
    'label'     => __( 'Text for donation button', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => 'Donate Now',
    'transport' => 'postMessage',
  );

  $options['donation_button_link'] = array(
    'id'    => 'donation_button_link',
    'label'     => __( 'Link for donation button', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text'
  );

  $options['donate_bg_color'] = array(
    'id'    => 'donate_bg_color',
    'label'     => __( 'Select the donation button color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $donation_bg,
    'transport' => 'postMessage',
  );

  $options['donate_border_color'] = array(
    'id'    => 'donate_border_color',
    'label'     => __( 'Select the donation button bottom border color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $donation_border,
    'transport' => 'postMessage',
  );

  $options['donate_bg_color_hover'] = array(
    'id'    => 'donate_bg_color_hover',
    'label'     => __( 'Select the donation button hover color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $donation_border,
  );

  $options['donate_border_color_hover'] = array(
    'id'    => 'donate_border_color_hover',
    'label'     => __( 'Select the donation button bottom border hover color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $donation_border,
  );

    $options['donate_button_animate'] = array(
        'id'    => 'donate_button_animate',
        'label'     => __( 'Donation button animation effect', 'mukabout' ),
        'section'   => $section,
        'type'      => 'select',
        'choices'   => $animate,
        'default'   => 'fadeInRight',
    );

  // Home Top Widgets
  $section = 'home-top-widgets';

  $sections[] = array(
    'id'      => $section,
    'title'     => __( 'Home Top Widgets', 'mukabout' ),
    'priority'    => '40',
    'description'   => __( 'This section will display on the home page once widgets are added to it in the widgets section.', 'mukabout' ),
    'panel'     => 'theme_options'
  );

  $options['home_top_widgets_bg'] = array(
    'id'    => 'home_top_widgets_bg',
    'label'     => __( 'Select the home top widgets background color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $home_top_widgets_bg,
    'transport' => 'postMessage',
  );

  $options['home_top_widgets_hover'] = array(
    'id'    => 'home_top_widgets_hover',
    'label'     => __( 'Select the home top widgets text hover color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $home_top_widgets_hover
  );

    $options['home_top_widgets_animation'] = array(
        'id'    => 'home_top_widgets_animation',
        'label'     => __( 'Home top widget section animation effect', 'mukabout' ),
        'section'   => $section,
        'type'      => 'select',
        'choices'   => $animate,
        'default'   => 'none',
    );

    // Home Events
  $section = 'home-events';

  $sections[] = array(
    'id'      => $section,
    'title'     => __( 'Home Events', 'mukabout' ),
    'priority'    => '40',
    'description'   => __( 'Home events section options', 'mukabout' ),
    'panel'     => 'theme_options'
  );

    $options['home_events_animation'] = array(
        'id'    => 'home_events_animation',
        'label'     => __( 'Home events section animation effect', 'mukabout' ),
        'section'   => $section,
        'type'      => 'select',
        'choices'   => $animate,
        'default'   => 'none',
    );

  $options['home_events_img_border'] = array(
    'id'    => 'home_events_img_border',
    'label'     => __( 'Select to display event images as square.', 'mukabout' ),
    'section'   => $section,
    'type'      => 'checkbox',
    'default'   => 0,
  );

  // Home News
  $section = 'home-news';

  $sections[] = array(
    'id'      => $section,
    'title'     => __( 'Home News', 'mukabout' ),
    'priority'    => '40',
    'description'   => __( 'Home blog posts options', 'mukabout' ),
    'panel'     => 'theme_options'
  );

  $options['home_news_show'] = array(
    'id'    => 'home_news_show',
    'label'     => __( 'Display home news section', 'mukabout' ),
    'section'   => $section,
    'type'      => 'checkbox',
    'default'   => 0,
  );

  $options['home_news_title'] = array(
    'id'    => 'home_news_title',
    'label'     => __( 'Title of the home page news section', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => 'Latest News Releases',
    'transport' => 'postMessage',
  );

  $options['home_blog_category'] = array(
    'id'    => 'home_blog_category',
    'label'     => __( 'Category for your home blog posts', 'mukabout' ),
    'section'   => $section,
    'type'      => 'select',
    'choices'   => $cats,
  );

  $options['home_blog_num'] = array(
    'id'    => 'home_blog_num',
    'label'     => __( 'Number of posts to display', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '1',
  );

  $options['home_news_link'] = array(
    'id'    => 'home_news_link',
    'label'     => __( 'Link to blog page', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );

  $options['home_news_button_text'] = array(
    'id'    => 'home_news_button_text',
    'label'     => __( 'Blog page button text', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => 'View all news',
    'transport' => 'postMessage',
  );

    $options['home_news_animation'] = array(
        'id'    => 'home_news_animation',
        'label'     => __( 'Home news section animation effect', 'mukabout' ),
        'section'   => $section,
        'type'      => 'select',
        'choices'   => $animate,
        'default'   => 'none',
    );

  // Home Gallery
  $section = 'home-gallery';

  $sections[] = array(
    'id'      => $section,
    'title'     => __( 'Home carousel', 'mukabout' ),
    'priority'    => '50',
    'description'   => __( 'Activate the themes gallery carousel plugin to display gallery images on the home page', 'mukabout' ),
    'panel'     => 'theme_options',
  );

  $options['home_carousel'] = array(
    'id'    => 'home_carousel',
    'label'     => __( 'Display home gallery section', 'mukabout' ),
    'section'   => $section,
    'type'      => 'checkbox',
    'default'   => 1,
  );

  $textPos = array(
    'carousel_pos_left' => 'Left',
    'carousel_pos_right' => 'Right',
  );

  //
  //Img One
  //
  $options['carousel_image_one'] = array(
    'id' => 'carousel_image_one',
    'label'   => __( 'Picture One', 'mukabout' ),
    'section' => $section,
    'type'    => 'image',
    'default' => ''
  );
  //title
  $options['carousel_title_one'] = array(
    'id' => 'carousel_title_one',
    'label'   => __( 'Title for picture one', 'mukabout' ),
    'section' => $section,
    'type'    => 'text',
  );
  //title color
  $options['carousel_title_one_color'] = array(
    'id' => 'carousel_title_one_color',
    'label'   => __( 'Title color for picture one', 'mukabout' ),
    'section' => $section,
    'type'    => 'color',
    'default'   => $white
  );
  //disc
  $options['carousel_disc_one'] = array(
    'id' => 'carousel_disc_one',
    'label'   => __( 'Description for picture one', 'mukabout' ),
    'section' => $section,
    'type'    => 'textarea',
  );
  //disc color
  $options['carousel_disc_one_color'] = array(
    'id' => 'carousel_disc_one_color',
    'label'   => __( 'Description color for picture one', 'mukabout' ),
    'section' => $section,
    'type'    => 'color',
    'default'   => $white
  );
  //postion
  $options['carousel_pos_one'] = array(
    'id' => 'carousel_pos_one',
    'label'   => __( 'Posistion of text', 'mukabout' ),
    'section' => $section,
    'type'    => 'radio',
    'choices' => $textPos,
    'default' => 'carousel_pos_left'
  );

  //
  //Img two
  //
  $options['carousel_image_two'] = array(
    'id' => 'carousel_image_two',
    'label'   => __( 'Picture two', 'mukabout' ),
    'section' => $section,
    'type'    => 'image',
    'default' => ''
  );
  //title
  $options['carousel_title_two'] = array(
    'id' => 'carousel_title_two',
    'label'   => __( 'Title for picture two', 'mukabout' ),
    'section' => $section,
    'type'    => 'text',
  );
  //title color
  $options['carousel_title_two_color'] = array(
    'id' => 'carousel_title_two_color',
    'label'   => __( 'Title color for picture two', 'mukabout' ),
    'section' => $section,
    'type'    => 'color',
    'default'   => $white
  );
  //disc
  $options['carousel_disc_two'] = array(
    'id' => 'carousel_disc_two',
    'label'   => __( 'Description for picture two', 'mukabout' ),
    'section' => $section,
    'type'    => 'textarea',
  );
  //disc color
  $options['carousel_disc_two_color'] = array(
    'id' => 'carousel_disc_two_color',
    'label'   => __( 'Description color for picture two', 'mukabout' ),
    'section' => $section,
    'type'    => 'color',
    'default'   => $white
  );
  //postion
  $options['carousel_pos_two'] = array(
    'id' => 'carousel_pos_two',
    'label'   => __( 'Posistion of text', 'mukabout' ),
    'section' => $section,
    'type'    => 'radio',
    'choices' => $textPos,
    'default' => 'carousel_pos_left'
  );
  //
  //Img three
  //
  $options['carousel_image_three'] = array(
    'id' => 'carousel_image_three',
    'label'   => __( 'Picture three', 'mukabout' ),
    'section' => $section,
    'type'    => 'image',
    'default' => ''
  );
  //title
  $options['carousel_title_three'] = array(
    'id' => 'carousel_title_three',
    'label'   => __( 'Title for picture three', 'mukabout' ),
    'section' => $section,
    'type'    => 'text',
  );
  //title color
  $options['carousel_title_three_color'] = array(
    'id' => 'carousel_title_three_color',
    'label'   => __( 'Title color for picture three', 'mukabout' ),
    'section' => $section,
    'type'    => 'color',
    'default'   => $white
  );
  //disc
  $options['carousel_disc_three'] = array(
    'id' => 'carousel_disc_three',
    'label'   => __( 'Description for picture three', 'mukabout' ),
    'section' => $section,
    'type'    => 'textarea',
  );
  //disc color
  $options['carousel_disc_three_color'] = array(
    'id' => 'carousel_disc_three_color',
    'label'   => __( 'Description color for picture three', 'mukabout' ),
    'section' => $section,
    'type'    => 'color',
    'default'   => $white
  );
  //postion
  $options['carousel_pos_three'] = array(
    'id' => 'carousel_pos_three',
    'label'   => __( 'Posistion of text', 'mukabout' ),
    'section' => $section,
    'type'    => 'radio',
    'choices' => $textPos,
    'default' => 'carousel_pos_left'
  );
  //
  //Img four
  //
  $options['carousel_image_four'] = array(
    'id' => 'carousel_image_four',
    'label'   => __( 'Picture four', 'mukabout' ),
    'section' => $section,
    'type'    => 'image',
    'default' => ''
  );
  //title
  $options['carousel_title_four'] = array(
    'id' => 'carousel_title_four',
    'label'   => __( 'Title for picture four', 'mukabout' ),
    'section' => $section,
    'type'    => 'text',
  );
  //title color
  $options['carousel_title_four_color'] = array(
    'id' => 'carousel_title_four_color',
    'label'   => __( 'Title color for picture four', 'mukabout' ),
    'section' => $section,
    'type'    => 'color',
    'default'   => $white
  );
  //disc
  $options['carousel_disc_four'] = array(
    'id' => 'carousel_disc_four',
    'label'   => __( 'Description for picture four', 'mukabout' ),
    'section' => $section,
    'type'    => 'textarea',
  );
  //disc color
  $options['carousel_disc_four_color'] = array(
    'id' => 'carousel_disc_four_color',
    'label'   => __( 'Description color for picture four', 'mukabout' ),
    'section' => $section,
    'type'    => 'color',
    'default'   => $white
  );
  //postion
  $options['carousel_pos_four'] = array(
    'id' => 'carousel_pos_four',
    'label'   => __( 'Posistion of text', 'mukabout' ),
    'section' => $section,
    'type'    => 'radio',
    'choices' => $textPos,
    'default' => 'carousel_pos_left'
  );
  // $options['home_gallery_title'] = array(
  //   'id'    => 'home_gallery_title',
  //   'label'     => __( 'Title of the home page image gallery section', 'mukabout' ),
  //   'section'   => $section,
  //   'type'      => 'text',
  //   'default'   => 'Latest Gallery Images',
  //   'transport' => 'postMessage',
  // );

  // $options['home_gallery_qty'] = array(
  //   'id'    => 'home_gallery_qty',
  //   'label'     => __( 'Number of gallery images to display on home page', 'mukabout' ),
  //   'section'   => $section,
  //   'type'      => 'text',
  //   'default'   => '3',
  // );

  // $options['home_gallery_link'] = array(
  //   'id'    => 'home_gallery_link',
  //   'label'     => __( 'Enter the link to your gallery page', 'mukabout' ),
  //   'section'   => $section,
  //   'type'      => 'text',
  //   'default'   => '',
  // );

  // $options['home_gallery_button_text'] = array(
  //   'id'    => 'home_gallery_button_text',
  //   'label'     => __( 'Enter the text for your gallery button', 'mukabout' ),
  //   'section'   => $section,
  //   'type'      => 'text',
  //   'default'   => 'View All Images',
  //   'transport' => 'postMessage',
  // );

  //   $options['home_gallery_animation'] = array(
  //       'id'    => 'home_gallery_animation',
  //       'label'     => __( 'Home gallery section animation effect', 'mukabout' ),
  //       'section'   => $section,
  //       'type'      => 'select',
  //       'choices'   => $animate,
  //       'default'   => 'none',
  //   );

  // Typography
  $section = 'typography';
  $font_choices = customizer_library_get_font_choices();

  $sections[] = array(
    'id'    => $section,
    'title'   => __( 'Typography', 'mukabout' ),
    'priority'  => '60',
    'panel'   => 'theme_options',
  );

  $options['primary-font'] = array(
    'id'    => 'primary-font',
    'label'     => __( 'Header Font', 'mukabout' ),
    'section'   => $section,
    'type'      => 'select',
    'choices'   => $font_choices,
    'default'   => 'Open Sans',
    'transport' => 'postMessage',
  );

  $options['primary-font-color'] = array(
    'id'    => 'primary-font-color',
    'label'     => __( 'Header Font Color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $home_top_widgets_bg,
    'transport' => 'postMessage',
  );

  $options['secondary-font'] = array(
    'id'    => 'secondary-font',
    'label'     => __( 'Paragraph Font', 'mukabout' ),
    'section'   => $section,
    'type'      => 'select',
    'choices'   => $font_choices,
    'default'   => 'Open Sans',
    'transport' => 'postMessage',
  );

  $options['link_hover_color'] = array(
    'id'    => 'link_hover_color',
    'label'     => __( 'Post/Page Content link hover color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $primary_green,
  );

  $options['button_color'] = array(
    'id'    => 'button_color',
    'label'     => __( 'Sitewide buttons color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $primary_green,
    'transport' => 'postMessage',
  );

  $options['main_button_hover'] = array(
    'id'    => 'main_button_hover',
    'label'     => __( 'Sitewide button hover color', 'mukabout' ),
    'section'   => $section,
    'type'      => 'color',
    'default'   => $primary_green_hover,
  );

  //
  //  
  // Footer
  $section = 'footer';

  $sections[] = array(
    'id'      => $section,
    'title'     => __( 'Footer', 'mukabout' ),
    'priority'    => '70',
    'description'   => __( 'Footer area options', 'mukabout' ),
    'panel'     => 'theme_options',
  );

  $options['google_maps'] = array(
    'id'    => 'google_maps',
    'label'     => __( 'Google Maps', 'mukabout' ),
    'section' => $section,
    'type'    => 'checkbox',
    'default' => 1,
  );

  //   $options['footer_social_animation'] = array(
  //       'id'    => 'footer_social_animation',
  //       'label'     => __( 'Home social icons animation effect', 'mukabout' ),
  //       'section'   => $section,
  //       'type'      => 'select',
  //       'choices'   => $animate,
  //       'default'   => 'none',
  //   );

  // $options['footer_social_color'] = array(
  //   'id'    => 'footer_social_color',
  //   'label'     => __( 'Social Icons Color', 'mukabout' ),
  //   'section'   => $section,
  //   'type'      => 'color',
  //   'default'   => $social_icon,
  //   'transport' => 'postMessage',
  // );

  // $options['footer_social_color_hover'] = array(
  //   'id'    => 'footer_social_color_hover',
  //   'label'     => __( 'Social Icons Color Hover', 'mukabout' ),
  //   'section'   => $section,
  //   'type'      => 'color',
  //   'default'   => $white,
  // );


  // $options['footer_copyright'] = array(
  //   'id'    => 'footer_copyright',
  //   'label'     => __( 'Enter footer copyright text.', 'mukabout' ),
  //   'section'   => $section,
  //   'type'      => 'textarea',
  //   'default'   => '&#169; Copyright',
  // );

  // Social Media Icons
  $section = 'social-icons';

  $sections[] = array(
    'id'      => $section,
    'title'     => __( 'Social Icons', 'mukabout' ),
    'priority'    => '80',
    'description'   => __( 'Add your social media links', 'mukabout' ),
    'panel'     => 'theme_options',
  );

  $options['twitter_link'] = array(
    'id'    => 'twitter_link',
    'label'     => __( 'Enter a Twitter link', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );

  $options['facebook_link'] = array(
    'id'    => 'facebook_link',
    'label'     => __( 'Enter a Facebook link', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );

  $options['google_link'] = array(
    'id'    => 'google_link',
    'label'     => __( 'Enter a Google link', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );

  $options['instagram_link'] = array(
    'id'    => 'instagram_link',
    'label'     => __( 'Enter a Instagram link', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );

  $options['vimeo_link'] = array(
    'id'    => 'vimeo_link',
    'label'     => __( 'Enter a Vimeo link', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );

  $options['youtube_link'] = array(
    'id'    => 'youtube_link',
    'label'     => __( 'Enter a Youtube link', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );

  $options['pinterest_link'] = array(
    'id'    => 'pinterest_link',
    'label'     => __( 'Enter a Pinterest link', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );

  $options['linkedin_link'] = array(
    'id'    => 'linkedin_link',
    'label'     => __( 'Enter a LinkedIn link', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );
  

  // Contact Info
  $section = 'Contact Info';

  $sections[] = array(
    'id'      => $section,
    'title'     => __( 'Contact Info', 'mukabout' ),
    'priority'    => '90',
    'description'   => __( 'Add you contact info', 'mukabout' ),
    'panel'     => 'theme_options',

  );

  $options['address'] = array(
    'id'    => 'contact_address',
    'label'     => __( 'Address', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );

  $options['telephone'] = array(
    'id'    => 'contact_telephone',
    'label'     => __( 'Telephone', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );
  $options['email'] = array(
    'id'    => 'contact_email',
    'label'     => __( 'Email', 'mukabout' ),
    'section'   => $section,
    'type'      => 'text',
    'default'   => '',
  );


  // Custom CSS
  $section = 'Custom-css';

  $sections[] = array(
    'id'      => $section,
    'title'     => __( 'Custom CSS', 'mukabout' ),
    'priority'    => '100',
    'description'   => __( 'Enter custom CSS styles', 'mukabout' ),
    'panel'     => 'theme_options',
  );

  $options['custom_css_textarea'] = array(
    'id'      => 'custom_css_textarea',
    'label'       => __( 'Custom CSS', 'mukabout' ),
    'section'     => $section,
    'type'        => 'textarea',
    'default'     => ''
  );


  // Adds the sections to the $options array
  $options['sections'] = $sections;

  $customizer_library = Customizer_Library::Instance();
  $customizer_library->add_options( $options );

  // To delete custom mods use: customizer_library_remove_theme_mods();

}
add_action( 'init', 'customizer_library_mukabout_options' );