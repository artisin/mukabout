<?php
/**
 * Template Name: Single Active SideNav
 */
?>


<?php 

  $args = array( 
    'orderby' => 'date',
    'post_type' => 'post',
    'category_name' => 'news'
  );
  $the_query = new WP_Query( $args );

?>

<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

  <?php 
    //get_template_part('templates/page', 'header'); 
  ?>
  <?php 
    //get_template_part('templates/content', 'page'); 
  ?>

<?php endwhile; else: ?>

  <p>Sorry, there are no posts to display</p>

<?php endif; ?>
