require('imports?define=>false!fastclick')(document.body);
require('foundation-sites');
//top bar
// require('foundation-sites/js/foundation/foundation.topbar.js');
require('./vendor/foundation.topbar.js');
require('foundation-sites/js/foundation/foundation.equalizer.js');
// require('./vendor/slabtext.js');

require('gsap/src/uncompressed/TweenMax.js');

//local assets
require('./vendor/slick.js');
// require('./vendor/hoverIntent.js');
require('waypoints/lib/jquery.waypoints.js');
require('waypoints/lib/shortcuts/sticky.js');
