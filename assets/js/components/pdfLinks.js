'use strict';

exports.init = function() {
  findLinks();
};


function findLinks () {
  var links = $('main').find('a');
  $(links).each(function (i, el) {
    if (el.href.match(/(?:.pdf)+$/)) {
      var link = '<a class="pdfLinks" href="' + el.href + '">pdf</a>';
      $(el).after('<span>' + link + '</span>');
    }
  });
}
