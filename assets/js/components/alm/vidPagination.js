'use strict';

exports.init = function (data) {
  initVid(data);
};

function initVid(data) {
  var selector = data.content.context,
      url = $(selector).find('video').data('youtube');
  loadVid(url);
  //reloads vid on click
  $(selector).find('.alm-paging li').on('click', function(e) {
    e.preventDefault();
    //on ajax complete load
    $.fn.almComplete = function(){
      url = $(selector).find('video').data('youtube');
      loadVid(url);
    };
  });
}

function loadVid(url) {
  if (url) {
    videojs(document.getElementsByClassName("video-js")[0], {
      techOrder: ["youtube"], 
      src: url,
      quality: 480,
      ytcontrols: true
    }, function() {
      // auto play
      // this.play(); 
      //on play
      this.on('play', function () {
      });
      //on end
      this.on('ended', function() {
      });
    });
  }else{
    //For handeling vids that have been inited but reloaded via ajax
    videojs(document.getElementsByClassName("video-js")[0], {}, function() {});
  }
}