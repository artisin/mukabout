'use strict';

//export to call
exports.init = function (options) {
  CreateListPanel(options);
};

//proto creator
var CreateListPanel = function (options) {
  function ListPanel () {}
  ListPanel.prototype = listProtoype;
  var f = new ListPanel();
  f.init(options);
  return f;
};




var listProtoype = {
  init: function(options) {
    this.id = options.id;
    this.domId = options.domId;
    this.monthColor = {
      jan: 'rgba(63, 167, 252, 1)',
      feb: 'rgba(249, 71, 169, 1)',
      mar: 'rgba(35,169,22, 1)', 
      apr: 'rgba(162,89,207, 1)',
      may: 'rgba(247,217,45, 1)',
      jun: 'rgba(145,145,145, 1)',
      jul: 'rgba(46,82,130, 1)',
      aug: 'rgba(51,196,153, 1)',
      sep: 'rgba(248,172,37, 1)',
      oct: 'rgba(246,88,41, 1)',
      nov: 'rgba(117,68,12, 1)',
      dec: 'rgba(205,21,19, 1)'
    };
    this.tl = new TimelineLite();
    this.animFinished = true;
    this.panId = '#'+this.domId;
    this.wrapper = $(this.panId).find('#content_wrapper');
    this.wrapperHeight = $(this.wrapper).outerHeight();
    this.displayMonth = $(this.panId).find('.displayMonth');
    this.maxHeaderHeight = 0;
    this.id = this.id;
    this.months = {};
    this.currentMonth = 0;
    var id = this.id;
    this.scrollData = {};
    this.config();
  },
  config: function () {
    var self = this,
        cardObjects = $(this.wrapper).find('.object'),
        maxHeaderHeight = self.maxHeaderHeight,
        currentMonth = self.currentMonth,
        wrapperHeight = self.wrapperHeight,
        scrollData = self.scrollData,
        displayMonth = self.displayMonth,
        wrapper = self.wrapper, 
        months = self.months;
    self.cardCount = cardObjects.length;

    var setAlpha = function (color, alpha) {
      return self.setAlpha(color, alpha);
    };

    //header height equalizer
    //find header height
    cardObjects.each(function (i, card) { 
      var context = $(card).find('.context');
      var headerHeight = $(context).children('.title').outerHeight();
      maxHeaderHeight = headerHeight > maxHeaderHeight ? headerHeight : maxHeaderHeight;
    });
    //set header height
    cardObjects.each(function (i, card) { 
      $(card).find('.title').css('height', maxHeaderHeight);
    });
    //Indv cards for month
    cardObjects.each(function (i, card) {
      var month = $(card).children('.info').children('.date').children('.month'),
          monthText = $(month)[0].innerText.replace(/\s/g, ''),
          color = self.monthColor[monthText.toLowerCase()],
          offset = $(card)[0].offsetTop;
          // console.log($(card)[0].offsetTop)
      //congif of months
      if (months[monthText] === undefined && color) {
        //set month to track
        currentMonth = i;
        //month data
        months[monthText] = {
          color: color,
          containter: $(month),
          text: $(month).children('.monthText'),
          line: $(month).children('.monthLine'),
          offset: offset,
          lineHeight: $(card).outerHeight(true)
        };
      }else{
        //update height if newly added cards
        var tempMonth = months[monthText];
        // console.log(tempMonth)
        if (tempMonth) {
          tempMonth.lineHeight = tempMonth.lineHeight + $(card).outerHeight(true);
          months[monthText] = tempMonth;
        }
      }
      //cards for the month
      if (!$(card).hasClass('card_init')) {
        $(card).addClass('card_init');
        //set card styling
        //month
        $(card).find('.date').css('border-color', setAlpha(color, 0.8));
        //little cirlce
        $(card).find('.circle').css('background-color', setAlpha(color, 0.7));
        //card top
        var context = $(card).find('.context');
        $(context).css({
          'border-color': setAlpha(color, 0.8)
        });
        var readMore = $(card).find('.readMore');
        if (readMore.length) {
          $(card).find('.linkToArticle').remove();
        }
      }
    });

    //config line and scroll points
    Object.keys(months).forEach(function (key, index, arr) {
      var month = months[key],
          color = month.color;
      var lineHeight;
      var nxtMonth = months[arr[index + 1]];
      if (!index) {
        if (nxtMonth) {
          lineHeight = (nxtMonth.offset - month.offset) + 10;
        }else{
          //only one month
          lineHeight = wrapperHeight;
        }
      }else{
        if (nxtMonth) {
          lineHeight = (nxtMonth.offset - month.offset) + 10;
        }else{
          //last month
          lineHeight = month.lineHeight + wrapperHeight / 1.25;
        }
      }
      //Set scroll data
      var prevScroll = scrollData[index - 1];
      scrollData[index] = {
        scroll: prevScroll ? lineHeight + prevScroll.scroll : lineHeight,
        ref: displayMonth, 
        text: key,
        color: color
      };
      //sets lines
      $(month.containter).css({
        display: 'block'
      });
      if (index === 0) {
        if (!$(displayMonth).hasClass('panel_init')) {
          $(displayMonth).addClass('panel_init');
          $(displayMonth).css({
            'border-color': setAlpha(color, 0.9),
            'background': setAlpha(color, 0.7)
          });
          $(displayMonth).html( "<span>"+ key +"</span>" );
        }
      }
      $(month.line).css({
        display: 'block',
        background: color,
        height: lineHeight,
      });
    });

    scrollData.self = self;
    $(wrapper).bind('scroll', scrollData, self.onScroll);
  },
  setAlpha: function(color, alpha) {
    if (color) {
      alpha = alpha ? alpha : 0.5;
      return color.replace(/[^,]+(?=\))/, alpha);
    }
    return '#eee';
  },
  //Runs on scroll
  onScroll: function(evt) {
    var scrollData = evt.data,
        self = scrollData.self;

    var setAlpha = function (color, alpha) {
      return self.setAlpha(color, alpha);
    };

    
    var pos = $(evt.target).scrollTop(),
        current = 0;
    Object.keys(scrollData).forEach(function (elm, i) {
      var scrollPos = scrollData[elm].scroll; 
      if (scrollPos - 20 < pos) {
        current = i + 1;
      }
      return scrollPos;
    });
    //re-run config if more cards added
    $.fn.almComplete = function (alm) {
      if (alm.custom_id === self.id) {
        self.config();
      }
    };
    //scroll
    var data =  scrollData[current];
    if (data) {
      if ($(data.ref)[0].innerText !== data.text && self.animFinished) {
        self.animFinished = false;
        var span = $(data.ref).children('span');
        var changeMonth = function () {
          $(span).text(data.text);
        };
        var complete = function () {
          self.animFinished = true;
        };

        //text
        var tl = self.tl;
        tl.to(span, 0.15, {autoAlpha: 0, onComplete: changeMonth})
          .to(data.ref, 0.5, {
              borderColor: setAlpha(data.color, 0.9),
              backgroundColor: setAlpha(data.color, 0.7)
            })
          .to(span, 0.15, {autoAlpha: 1}, '-=0.15')
          .to(span, 0.15, {onComplete: complete});
      }
    }
  }
};

