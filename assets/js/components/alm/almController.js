'use strict';

var ASQ = require("asynquence"),
    listPannel = require('./listPannels'),
    vidPag = require('./vidPagination');

ASQ()
  .then(function (done) {
    var almObj = {};
    $.fn.almComplete = function (alm) {
      almObj[alm.custom_id] = alm;
    };
    var ajaxModuals = $('.alm-ajax').length,
        checkCount = 0,
        //Check for responses
        checkResponse = function () {
          checkCount++;
          if (checkCount > 100) {
            //Fail after 40sec if not loaded
            done.fail("Failed! 'Was not able to load ajax content!'");
          }else{
            //Wait till all responsed recived
            setTimeout(function () {
              if (Object.keys(almObj).length === ajaxModuals) {
                return done(almObj);
              }else{
                return checkResponse();
              }
            }, 400);
          }
        };
        //init call
        checkResponse();
  })
  .val(function (alm) {
    Object.keys(alm).forEach(function (id) {
      //List Pannels
      if (id.match(/list_pannel-/gi) && !(id.match(/no_date/gi))) {
        var domId = id.replace(/list_pannel-/, '');
        //init pannel
        listPannel.init({
          id: id,
          domId: domId
        });
      }else if (id.match(/vid/gi)) {
        vidPag.init(alm[id]);
      }
    });
  })
  //Error handler
  .or(function (err) {
    console.log(err);
  });
