"use strict";


exports.init = function() {
  configNav();
  configWatch();
};

exports.config = function(page) {
  configNav(page);
  configWatch(page);
};



//Helpers
function splitInto(a, n) {
  var len = a.length,
      out = [], 
      i = 0;
  while (i < len) {
    out.push(a.slice(i, i += n));
  }
  return out;
}


//Styling for header nav
function configNav (removeStyles) {
  var navWidth = $('#menu-main-page-nav').outerWidth();
  //Gets the postion of button for proper dropdow align
  var firstPos,
      navButton = $('#menu-main-page-nav').children('.top-level-tab'),
      navButtonPos = $.map($(navButton), function (val, i) {
        var pos = $(val).position().left;
        if (i === 0) {
          firstPos = pos;
          return 0;
        }else{
          return firstPos - pos;
        }
      });
  
  var configStyle = function(el, index) {
    var El = $(el);
    //count list iteams need to be %3
    var list = El.children('.menu-item'),
        count = $(list).length;
    //remvoe styles for moblie nav
    //REMOVE ??? Dedicated mobile nav
    if (removeStyles) {
      El.removeAttr('style');
      $(list).each(function (i) {
        if (i >= 2 && typeof(removeStyles) === 'string') {
          //instate inner page menu styles
          $(list[i]).attr('style', '');
          $(list[i]).css({
            'border-top': 'solid 3px rgba(56, 56, 56, 0.90)',
            'border-bottom': 'solid 3px rgba(56, 56, 56, 0.90)'
          });
        }
        //remove extra blank spaces
        if ($(list[i]).hasClass('menu-item-extra')) {
          $(list[i]).remove();
        }
      });
    }else{
      //set width + postion
      El.css({
        width: navWidth,
        "margin-left": navButtonPos[index] + "px"
      });
      //even count to fill empty li
      if (count % 3 !== 0) {
        var toAdd = 0;
        while(((toAdd + count) % 3) !== 0){
          toAdd++;
        }
        //toAdd x num
        for (var i = 0; i < toAdd; i++) {
          El.append('<li class="menu-item menu-item-extra"><a href="#">&nbsp;</a></li>');
        }
      }

      var listItems = El.children('.menu-item'),
          listLength = listItems.length;
      //Border Effects
      listItems.each(function (i, el) {
        el = $(el);
        var addBorder = function (bd, outside, options) {
          var width = !outside ? '3px' : '8px',
              type = !outside ? 'solid' : 'solid',
              color = !outside ? 'rgba(111, 146, 174, 0.67)' : '#6f92ae',
              style = width + " " + type + " " + color;
          //top
          if (bd === 'top') {
            el.css({
              "border-top": style
            });
            if (options) {
              el.css(options);
            }
          }
          //bottom
          if (bd === 'bottom') {
            el.css({
              "border-bottom": style 
            });
          }
          //left
          if (bd === 'left') {
            el.css({
              "border-left": style 
            });
          }
          //right
          if (bd === 'right') {
            el.css({
              "border-right": style 
            });
          }
        };

        //bd Top
        if (i >= 3) {
          addBorder('top');
        }else{
          addBorder('top', false, {
            "padding-top": '0px' 
          });
        }
        //bd Bottom
        if (i < listLength - 3) {
          addBorder('bottom');
        }else{
          // addBorder('bottom', true);
        }
        //bd left
        if (i % 3) {
          addBorder('left');
        }else{
          addBorder('left', true);
        }
        //bd right
        if ((i + 1 ) % 3) {
          addBorder('right');
        }else{
          addBorder('right', true);
        }
      });
    }
  };

  /**
   * Configs all the navTabs styles and sub-menu
   * @param  {$} navList - for all tabs
   * @return -Styles on dom
   */
  var configAll = function (navList) {
    //set nav widths
    $(navList).each(function(index, el) {
      configStyle(el, index);
      //reflow on last
      if (el === navList.length - 1) {
        $(document).foundation('topbar', 'reflow');
      }
    });
  };

  //single style remove
  if (typeof(removeStyles) === 'string') {
    var domClass = '.' + removeStyles;
    configStyle($('li' + domClass ).children('ul'));
  }else{
    //config all or remove all
    configAll($(navButton).children('ul'));
  }

}


/**
 * Widow resize config
 */
$( window ).resize(function() {
  if ($( window ).width() > 650) {
    configNav();
  }else{
    configNav(true);
  }
});

var subHeaderPos = $('.sub-header_btm-shadow')[0].offsetTop,
    fillerHidden = false;
var configSingleMenu = function (subMenu) {
  subMenu = $(subMenu).children('ul');
  var subList = $(subMenu).find('.side-nav').children('.single-no-children');
  //remove inline style
  $(subList).attr('style', '');

  // console.log(subList)
  //congif link heights
  var linkCount = subList.length + 1,
      vh = window.innerHeight - subHeaderPos,
      auxHeight = 0;

  //sets height of sublist for nav
  if ((vh / linkCount) >= 36) {
    //defualt
    auxHeight = window.innerHeight - (linkCount * 36);
  }else{
    subList.height(vh / linkCount);
  }
  //set aux side menu height
  $('.auxSingleMenu').height(auxHeight + 50);
  //set min height to advoid jank on sticky nav scroll
  $('.content_Wrapper').css({
    'min-height': $('#sideNav_Wrapper').outerHeight()
  });

  //Sticky nav on scroll
  var stickyNav = new Waypoint({
    element: '.sub-header_btm-shadow',
    handler: function(dir) {
      if (dir === 'down') {
        $('#sideNav_Wrapper').css({
          position: 'fixed',
          top: 51
        });
        $('.page_Content').css({
          'margin-left': 225
        });
      }else{
        $('#sideNav_Wrapper').attr('style', '');
        $('.page_Content').css({
          'margin-left': 0
        });
      }
    }
  });
};

/**
 * Header Animation on scroll
 */
var headerShrunk = false,
    headerAnimRunnig = false;
var headerAnim = function () {
  var topBarCont = $('.top-bar-container'),
      topBarNav = $(topBarCont).children('.top-bar'),
      navTabs   = $('#menu-main-page-nav').children('li').children('a'),
      tl = new TimelineMax(),
      complete = function (removeBg) {
        headerAnimRunnig = false;
        if (removeBg) {
          $(topBarCont).removeClass('showNavBgPattern');
        }else{
          $(topBarCont).addClass('showNavBgPattern');
        }
      };


  if (window.scrollY > 10 && !headerShrunk) {
    //shrink
    headerAnimRunnig = true;
    headerShrunk = true;
    tl.set('.menuDropShadow', {opacity: 0, top: "50px"})
      .to(navTabs, 0.3, {
        borderTop: "0px solid rgba(77,77,77,0.7)",
        borderRadius: "0px",
        height: "50px"
      })
      .to(topBarNav, 0.3, {marginTop: '0px'})
      .to('.top-bar-container', 0.3, {height: "51px", })
      .to('.menuDropShadow', 0.2, {
        opacity: 1,
        onComplete: complete
      }, "-=0.2");
  }else if (window.scrollY < 10 && headerShrunk) {
    //size up
    headerAnimRunnig = true;
    headerShrunk = false;
    tl.to('.menuDropShadow', 0.2, {opacity: 0})
      .to('.menuDropShadow', 0, {top: "80px"})
      .to('.top-bar-container', 0.3, {height: "80px"})
      .to(topBarNav, 0.3, {marginTop: '15px'})
      .to(navTabs, 0.3, {
        borderTop: "8px solid rgba(77,77,77,0.7)",
        borderRadius: "4px",
        height: "65px",
      }, "-=0.3")
      .to(topBarCont, 0.2, {opacity: 1})
      .to('.menuDropShadow', 0.2, {
        opacity: 0.4, 
        onComplete: complete,
        onCompleteParams: [true]
      }, "-=0.2");

  }
};
var throttle = require('lodash.throttle');
//need to throttle for anim to work properly
$(window).on('scroll', throttle(function () {
  if (!headerAnimRunnig) {
    headerAnim();
  }
}, 100, {leading: false}));


/**
 * hover animation and state 
 */
var activeMenuHover = false,
    navTabs = $('nav').find('.navTab');
var initMenuTab = function (e) {
  e.preventDefault();
  var tl = new TimelineMax(),
      type = e.type,
      navLinks = $(e.currentTarget).children('ul').children('.menu-item').children('a'),
      navLinkArray = splitInto(navLinks, 3),
      linkContainer = $(e.currentTarget).find('.sub-menu');
  var removeStyles = function () {
    $(linkContainer).removeClass('showDropDown');
    $(e.currentTarget).children('a').removeClass('activeMenuTab activeMenuTabShrunk');
    $(navLinks).removeAttr('style');
  };
  if (type === 'mouseleave' && !activeMenuHover) {
    $(navTabs).removeAttr('style');
    tl.to(linkContainer, 0.25, {opacity: 0, onComplete: removeStyles});
  }else{
    $(linkContainer).addClass('showDropDown');
    //hacky way to get proper z, prbl can be done with css
    $(navTabs).each(function (i, elm) {
      if ($(linkContainer).context !== $(elm).context) {
        $(elm).css('z-index', -20);
      }
    });
    if (headerShrunk) {
      $(e.currentTarget).children('a').addClass('activeMenuTabShrunk');
    }else{
      $(e.currentTarget).children('a').addClass('activeMenuTab');
    }
    tl.to(linkContainer, 0.25, {opacity: 1})
      .staggerTo(navLinkArray, 1, {color: '#4d4d4d'}, 0.1, "-=0.25");
  }
};

//Get id to bind to watch
var activeTab;
function configWatch (activeClass) {
  var navTabs = $('#menu-main-page-nav').children('.top-level-tab');
  $(navTabs).each(function (i, el) {
    var menuTabId = $(el).attr('class').split(/\s+/).filter(function (el) {
      if (el.match(/menu-item-\d/)) {
        el = '.' + el;
        //unbind event
        if ($(el).hasClass(activeClass || 'active')) {
          //unbind active
          activeTab = el;
          $(el).unbind('mouseenter mouseleave');
          $(el).children('a').addClass('singleMenuTab');
          //layout of the tabs so the sidenav tab is first
          if (1 !== 0) {
            $(navTabs[0]).before($(el));
            //resize and such
            configNav();
          }
          //config
          configSingleMenu($('#sideNav_Wrapper'));
        }else{
          return el;
        }
      }
    });
    if (menuTabId[0]) {
      var parentClass = "." + menuTabId[0];
      if (parentClass !== activeTab) {
        $(parentClass).bind('mouseenter mouseleave', initMenuTab);
      }
    }
  });
}
