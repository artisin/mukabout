//Twitter animation
//NEED BACK UP HOVER!
(function twitterFooter () {
  var tweetWidget = $('.tweeple-feed-widget'),
      tweets = $(tweetWidget).find('.tweets').children(),
      tweetWrapHeight = $(tweets).outerHeight() - 35,
      numOfTweets = tweets.length - 1,
      currentTweet = 0,
      tweetCount = $('.twitterFeed_footer').find('.tweetCount').children('span'),
      changerHover = false;

  var tweetChanger = $('.twitterFeed_footer').find('.tweetChanger');
  $(tweetChanger).hover(function (e) {
    e.preventDefault();
    changerHover = e.type === "mouseenter";
  });

  //Only run if tweets
  if (tweets.length) {
    //count
    $(tweetCount[1]).text("/"+tweets.length);
    $(tweetCount[0]).text(currentTweet + 1);
    var tl = new TimelineLite();
    var displayTweet = function (reverse) {
      // console.log(reverse)
      var activeTweet = $(tweets)[currentTweet],
          text = $(activeTweet).find('.tweet-text'),
          metaTime = $(activeTweet).find('.tweet-stamp'),
          metaUser = $(activeTweet).find('.tweet-author'),
          textHeight = $(text).outerHeight(),
          yAdj = (tweetWrapHeight - textHeight) / 2,
          tweetHover = false,
          overide = false;

      //see if user be hovering
      $(activeTweet).hover(function (e) {
        e.preventDefault();
        tweetHover = e.type === "mouseenter";
      });


      //fade out to next tweet
      var nextTweet = function () {
          //if user is hovering wait until they be done
          if ((!tweetHover && !changerHover) || overide) {
            //up currentTweet
            if (currentTweet >= numOfTweets) {
              //revert back to first
              currentTweet = 0;
            }else{
              currentTweet++;
            }
            //next
            tl.to(activeTweet, 0.75, {y: '200%', opacity: 0, ease: Sine.easeOut})
              .to(activeTweet, 0, {y: '0%', onComplete: displayTweet});
            
          }else{
            setTimeout(function () {
              nextTweet();
            }, 1000);
          }
      };

      //updates counter
      var updateCounter = function () {
        var textChange = function () {
          $(tweetCount[0]).text(currentTweet + 1);
        };
        TweenLite.to(tweetCount[0], 0.25, {opacity: 0, onComplete: textChange});
      };

      var revcCheck = function () {
        if (reverse) {
          return TweenLite.to(activeTweet, 0, {y: '200%'});
        }
        return;
      };

      //animation
      tl.to(text, 0, {opacity: 0, onComplete: revcCheck})
        .to([metaTime, metaUser], 0, {opacity: 0, bottom: -35, onComplete: updateCounter})
        .to(activeTweet, 0.5, {y: '100%', opacity: 1})
        .to(text, 0.5, {opacity: 1, marginTop: yAdj, ease: Sine.easeIn}, "-=0.5")
        .to(tweetCount[0], 0.5, {opacity: 1})
        .to([metaTime, metaUser], 0.35, {opacity: 1, bottom: 0}, "-=0.65")
        .to(text, 0, {onComplete: nextTweet}, '+=4.5');
    };
    //prev Tweet
    $(tweetChanger).children('.tweet_Prev').click(function (e) {
      e.preventDefault();
      tl.clear();
      var tlRemove = new TimelineLite(),
          activeTweet = currentTweet;
      currentTweet = currentTweet ? currentTweet - 1 : tweets.length - 1;
      //next
      tlRemove
        .to($(tweets)[activeTweet], 0.25, {y: '0%', opacity: 0})
        .to($(tweets)[activeTweet], 0, {
          y: '0%',  
          onComplete: displayTweet,
          onCompleteParams: [true]
        });
    });

    //next Tweet
    $(tweetChanger).children('.tweet_Next').click(function (e) {
      e.preventDefault();
      tl.clear();
      var tlRemove = new TimelineLite(),
          activeTweet = currentTweet;
      currentTweet = currentTweet >= tweets.length - 1 ? 0 : currentTweet + 1;
      //next
      tlRemove
        .to($(tweets)[activeTweet], 0.25, {y: '200%', opacity: 0})
        .to($(tweets)[activeTweet], 0, {y: '0%',  onComplete: displayTweet});
    });

    //main call
    displayTweet();
  }

})();