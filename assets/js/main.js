

/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
var count = 0;
(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        //calls
        this.foundation();
        this.nav();
        this.footer();
        this.almLoadMore();
        this.pdfLinks();
      },
      //foundation init
      foundation: function () {
        $(document).foundation();
      },
      //Nav Styling
      nav: function () {
        var nav = require('./components/primaryNav');
        nav.init();
      },
      //Looks for alm-ajax targets if any calls said loader
      almLoadMore: function () {
        if ($('#ajax-load-more').length) {
          require('./components/alm/almController');
        }
      },
      //Gives pdf tag on pdf links
      pdfLinks: function() {
        var links = require('./components/pdfLinks');
        links.init();
      },
      footer: function () {
        //twitter
        require('./components/footer/twitterFeed.js');
        //google Map
        require('./components/footer/googleMap.js');
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      },
    },
    //All subpage templates
    'subPageCommon': {
      init: function (page) {
        var nav = require('./components/primaryNav');
        //Config nav for said page
        nav.config(page);
      }
    },
    // Home page
    'home': {
      init: function() {
        require('./pages/home/homeAssets.js');
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    'business-development': {
      init: function() {
        var bis = require('./pages/bis/bisAssets.js');
        bis.init();
      }
    },
    'government-information': {
      init: function() {
        //assets
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';
      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');
      var bodyClasses = document.body.className.split(/\s+/),
          nonSubPage = ['home', 'about'],
          subPage = ['business-development', 'government-information'],
          isSubPage = true;
      // Fire page-specific init JS, and then finalize JS
      $.each(bodyClasses, function(i, classnm) {
        if (nonSubPage.indexOf(classnm) >= 0) {
          isSubPage = false;
        }
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });
      //run subPageCommon
      if (isSubPage) {
        //find specific sub page
        subPage.forEach(function (page) {
          if (bodyClasses.indexOf(page) >= 0) {
            UTIL.fire('subPageCommon', 'init', page);
          }
        });
      }
      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.


