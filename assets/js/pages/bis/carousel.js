'use strict';

var title = $('.titleBox_Container');
$(title).on('init', function(e, slick){
  var boxes = $(title).find('.box');
  boxes = $(boxes).filter(function (i, elm) {
    var clone = false;
    $(elm)[0].classList.forEach(function(className) {
      if (className === 'slick-cloned') {
        clone = true;
      }
    });
    if (!clone) {
      return elm;
    } 
  });

  //set height for some breather room
  $(title).find('.slick-track').css({
    'height': 165
  });
  
  //Animation up for boxes
  var sizeUp = function (cur) {
    var current = boxes[cur],
        next = $(current).next(),
        prev = $(current).prev();
    TweenMax.to(current, 0.65, {scaleX:1, scaleY:1, opacity: 1});
    //right
    TweenMax.to(prev, 0.4, {scaleX:0.6, scaleY:0.6, opacity: 0.6});
    TweenMax.to(next, 0.4, {scaleX:0.6, scaleY:0.6, opacity: 0.6});
    //move next, next out of view
    TweenMax.to($(next).next(), 0.4, {scaleX:0.6, scaleY:0.6, x: 400, opacity: 0.6});
    TweenMax.to($(prev).prev(), 0.4, {scaleX:0.6, scaleY:0.6, x: -400, opacity: 0.6});
  };
  //Animation down for boxes
  var sizeDown = function (cur) {
    var current = boxes[cur],
        next = $(current).next(),
        prev = $(current).prev();
    TweenMax.to(current, 0.4, {scaleX:0.6, scaleY:0.6, opacity: 0.6});
    TweenMax.to($(next).next(), 0.4, {x: 0});
    TweenMax.to($(prev).prev(), 0.4, {x: 0});
  };

  //init
  sizeUp(slick.currentSlide);
  $(this).on('beforeChange', function (e, slick, current) {
    sizeDown(current);
  });
  $(this).on('afterChange', function (e, slick, current) {
    sizeUp(current);
  });
});

//Title boexes
title.slick({
  slidesToShow: 1,
  asNavFor: '.carousel',
  centerMode: true,
  'dots': true,
  variableWidth: true,
  focusOnSelect: true
});
//Main sides
$('.carousel').slick({
  // autoplay: true,
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  fade: true,
  cssEase: 'linear',
  arrows: false,
  asNavFor: '.titleBox_Container'
});

