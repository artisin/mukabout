//carousel
var carousel = $('.carousel');
carousel.slick({
  // autoplay: true,
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  fade: true,
  cssEase: 'linear',
  arrows: true
});

String.prototype.width = function(font) {
  var f = font || '12px arial',
      o = $('<div>' + this + '</div>')
            .css({'position': 'absolute', 'float': 'left', 'white-space': 'nowrap', 'visibility': 'hidden', 'font': f})
            .appendTo($('body')),
      w = o.width();

  o.remove();

  return w;
};

var testText = document.getElementById("testText"),
    //avg px per character
    textWidth = (testText.clientWidth + 1) / 52,
    textHeight = testText.clientHeight  + 1;

//text fitting
$('.carousel_img').each(function (val, elm) {
  var elm = $(elm),
      children = elm.children();
  var titleWidth = 0;
  var titleHieght;
  children.each(function (v, el) {
    el = $(el);
    if (el[0].tagName === 'H2') {
      titleWidth = el.width();
      titleHieght = el.height();
    }else if(el[0].tagName === 'P' && titleWidth){
      //Folling, breaks text into parts for black backed bg on text
      var text = el[0].innerHTML,
          textArray = [[]],
          currentSize = 0;
      //split into arrays
      text.split(' ').forEach(function(str) {
        var lenSize = str.length * textWidth;
        currentSize = currentSize + lenSize;
        if (titleWidth > currentSize) {
          textArray[textArray.length - 1].push(str);
        }else{
          currentSize = lenSize;
          textArray.push([str]);
        }
      });
      //compose arrays
      var newString;
      textArray.forEach(function(str) {
        if (newString) {
          newString = newString +  '<span>' + str.join(' ') + ' </span> <br>';  
        }else{
          newString = '<span>' + str.join(' ') + ' </span> <br>';
        }
      });
      el.html(newString);
      el.css({
        width: titleWidth,
        top: titleHieght + 10
      });
    }
    //turn on opactiy since hidden on load
    el.css({
      opacity: 1
    });
  });
});
