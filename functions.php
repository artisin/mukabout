<?php
/**
 * Sage includes
 *
 * The $mukabout_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */

require_once('phar:///var/www/wordpress/wp-content/plugins/PhpConsole.phar');


$mukabout_includes = [
  'lib/utils.php',                 // Utility functions
  'lib/pluginActivation/pluginActivation.php',
  'lib/init.php',                  // Initial theme setup and constants
  'lib/wrapper.php',               // Theme wrapper class
  'lib/conditional-tag-check.php', // ConditionalTagCheck class
  'lib/config.php',                // Configuration
  'lib/assets.php',                // Scripts and stylesheets
  'lib/titles.php',                // Page titles
  'lib/extras.php',                // Custom functions
  'lib/customizer.php',            // Theme Customizer
  'lib/components/topBar.php',        // Top bar Nav
  'lib/components/breadcrumb.php',        // breadcrumb
  'lib/components/list_panel.php',        // list panes
  'lib/components/custom-post-meta.php',        // list panes

  'inc/include.php',

];

// if(function_exists('xdebug_disable')) { xdebug_disable(); }
foreach ($mukabout_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'mukabout'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

