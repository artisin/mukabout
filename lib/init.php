<?php

namespace Roots\Sage\Init;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/mukabout-translations
  load_theme_textdomain('mukabout', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');
  add_theme_support('menus');
  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  // https://gist.github.com/awshout/3943026
  register_nav_menus([
    'header' => __('Primary Navigation Left', 'mukabout'),
    'footer' => __( 'Footer', 'mukabout' ),
  ]);

  // Add post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Add post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Add HTML5 markup for captions
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list']);

  // Tell the TinyMCE editor to use a custom stylesheet
  add_editor_style(Assets\asset_path('styles/editor-style.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary Sidebar', 'mukabout'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer', 'mukabout'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  //List pannels for home page
  register_sidebar( array(
      'name'           => __( 'Home Left Area', 'mukabout' ),
      'id'             => 'home_left',
      'description'    => 'Home-page area for list pannel one',
      'before_widget'  => '<div id="%1$s" class="home_widget_left listPannel1 %2$s">',
      'after_widget'   => '</div>',
      'before_title'   => '<h3 class="widget-title">',
      'after_title'    => '</h3>',
  ) );
  register_sidebar( array(
      'name'           => __( 'Home Middle Area', 'mukabout' ),
      'id'             => 'home_middle',
      'description'    => 'Home-page area for list pannel two',
      'before_widget'  => '<div id="%1$s" class="home_widget_middle listPannel2 %2$s">',
      'after_widget'   => '</div>',
      'before_title'   => '<h3 class="widget-title">',
      'after_title'    => '</h3>',
  ) );
  register_sidebar( array(
      'name'           => __( 'Home Right Area', 'mukabout' ),
      'id'             => 'home_right',
      'description'    => 'Home-page area for list pannel three',
      'before_widget'  => '<div id="%1$s" class="home_widget_right listPannel3 %2$s">',
      'after_widget'   => '</div>',
      'before_title'   => '<h3 class="widget-title">',
      'after_title'    => '</h3>',
  ) );
  
  // register_sidebar( array(
  //     'name'           => __( 'Home Events Area', 'mukabout' ),
  //     'id'             => 'home_events',
  //     'before_widget'  => '<div id="%1$s" class="home_widget_events %2$s">',
  //     'after_widget'   => '</div>',
  //     'before_title'   => '<h3 class="widget-title">',
  //     'after_title'    => '</h3>',
  // ) );
  // register_sidebar( array(
  //     'name'           => __( 'Footer Left', 'mukabout' ),
  //     'id'             => 'footer_sidebar-1',
  //     'before_widget'  => '<aside id="%1$s" class="widget %2$s">',
  //     'after_widget'   => '</aside>',
  //     'before_title'   => '<h5 class="widget-title">',
  //     'after_title'    => '</h5>',
  // ) );
 
  register_sidebar( array(
      'name'           => __( 'Footer Twitter Feed', 'mukabout' ),
      'id'             => 'footer_sidebar-twitter',
      'before_widget'  => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'   => '</aside>',
      'before_title'   => '<h5 class="widget_footer_twitter">',
      'after_title'    => '</h5>',
  ) );
  register_sidebar( array(
      'name'           => __( 'Footer Middle', 'mukabout' ),
      'id'             => 'footer_sidebar-2',
      'before_widget'  => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'   => '</aside>',
      'before_title'   => '<h5 class="widget_footer_middle">',
      'after_title'    => '</h5>',
  ) );
  // register_sidebar( array(
  //     'name'           => __( 'Footer Google Maps', 'mukabout' ),
  //     'id'             => 'footer_sidebar-googlemaps',
  //     'before_widget'  => '<aside id="%1$s" class="widget %2$s">',
  //     'after_widget'   => '</aside>',
  //     'before_title'   => '<h5 class="widget_footer_googleMaps">',
  //     'after_title'    => '</h5>',
  // ) );
  // register_sidebar( array(
  //     'name'           => __( 'Footer Right', 'mukabout' ),
  //     'id'             => 'footer_sidebar-3',
  //     'before_widget'  => '<aside id="%1$s" class="widget %2$s">',
  //     'after_widget'   => '</aside>',
  //     'before_title'   => '<h5 class="widget_footer_right">',
  //     'after_title'    => '</h5>',
  // ) );
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');
