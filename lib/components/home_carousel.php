<?php if ( get_theme_mod( 'home_carousel', '' ) ) : ?> 
  <!-- Used to get text width for text bg -->
  <div id="testText">
      <p>abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ</p>
  </div>
  <!-- Carousel -->
  <div class="carousel">
  <?php $image_one = get_theme_mod( 'carousel_image_one', '' ); if ( $image_one  ) : ?>
    <?php
      $img_one = "background-image: url(".esc_url( $image_one ).")";
      $text_pos_one = get_theme_mod( 'carousel_pos_one', '' );
      $title_color_one = get_theme_mod( 'carousel_title_one_color' );
      $disc_color_one = get_theme_mod( 'carousel_disc_one_color' );
      $dt1 = "color:".$title_color_one;
      $dc1 = "color:".$disc_color_one;
    ?>

    <div class="carousel_img"   style="<?php echo $img_one; ?>">
      <!-- Title -->
      <?php $title_one = get_theme_mod( 'carousel_title_one', '' ); if ( $title_one  ) : ?>
          <h2 id = "<?php echo $text_pos_one; ?>" style = "<?php echo $dt1; ?>"> <?php echo $title_one; ?></h2>
      <?php endif; ?>
      <!-- decs -->
      <?php $disc_one = get_theme_mod( 'carousel_disc_one', '' ); if ( $disc_one  ) : ?>
          <p id = "<?php echo $text_pos_one; ?>" style = "<?php echo $dc1 ?>"><?php echo $disc_one; ?></p>
      <?php endif; ?>
    </div>
  <?php endif; ?>
  
  <!-- Image Two -->
  <?php $image_two = get_theme_mod( 'carousel_image_two', '' ); if ( $image_two  ) : ?>
    <?php
      $img_two = "background-image: url(".esc_url( $image_two ).")";
      $text_pos_two = get_theme_mod( 'carousel_pos_two', '' );
      $title_color_two = get_theme_mod( 'carousel_title_two_color' );
      $disc_color_two = get_theme_mod( 'carousel_disc_two_color' );
      $dt1 = "color:".$title_color_two;
      $dc1 = "color:".$disc_color_two;
    ?>
    <div class="carousel_img"   style="<?php echo $img_two; ?>">
      <!-- Title -->
      <?php $title_two = get_theme_mod( 'carousel_title_two', '' ); if ( $title_two  ) : ?>
          <h2 id = "<?php echo $text_pos_two; ?>" style = "<?php echo $dt1; ?>"> <?php echo $title_two; ?></h2>
      <?php endif; ?>
      <!-- decs -->
      <?php $disc_two = get_theme_mod( 'carousel_disc_two', '' ); if ( $disc_two  ) : ?>
          <p id = "<?php echo $text_pos_two; ?>" style = "<?php echo $dc1 ?>"><?php echo $disc_two; ?></p>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <!-- Image three -->
  <?php $image_three = get_theme_mod( 'carousel_image_three', '' ); if ( $image_three  ) : ?>
    <?php
      $img_three = "background-image: url(".esc_url( $image_three ).")";
      $text_pos_three = get_theme_mod( 'carousel_pos_three', '' );
      $title_color_three = get_theme_mod( 'carousel_title_three_color' );
      $disc_color_three = get_theme_mod( 'carousel_disc_three_color' );
      $dt1 = "color:".$title_color_three;
      $dc1 = "color:".$disc_color_three;
    ?>
    <div class="carousel_img"   style="<?php echo $img_three; ?>">
      <!-- Title -->
      <?php $title_three = get_theme_mod( 'carousel_title_three', '' ); if ( $title_three  ) : ?>
          <h2 id = "<?php echo $text_pos_three; ?>" style = "<?php echo $dt1; ?>"> <?php echo $title_three; ?></h2>
      <?php endif; ?>
      <!-- decs -->
      <?php $disc_three = get_theme_mod( 'carousel_disc_three', '' ); if ( $disc_three  ) : ?>
          <p id = "<?php echo $text_pos_three; ?>" style = "<?php echo $dc1 ?>"><?php echo $disc_three; ?></p>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <!-- Image four -->
  <?php $image_four = get_theme_mod( 'carousel_image_four', '' ); if ( $image_four  ) : ?>
    <?php
      $img_four = "background-image: url(".esc_url( $image_four ).")";
      $text_pos_four = get_theme_mod( 'carousel_pos_four', '' );
      $title_color_four = get_theme_mod( 'carousel_title_four_color' );
      $disc_color_four = get_theme_mod( 'carousel_disc_four_color' );
      $dt1 = "color:".$title_color_four;
      $dc1 = "color:".$disc_color_four;
    ?>
    <div class="carousel_img"   style="<?php echo $img_four; ?>">
      <!-- Title -->
      <?php $title_four = get_theme_mod( 'carousel_title_four', '' ); if ( $title_four  ) : ?>
          <h2 id = "<?php echo $text_pos_four; ?>" style = "<?php echo $dt1; ?>"> <?php echo $title_four; ?></h2>
      <?php endif; ?>
      <!-- decs -->
      <?php $disc_four = get_theme_mod( 'carousel_disc_four', '' ); if ( $disc_four  ) : ?>
          <p id = "<?php echo $text_pos_four; ?>" style = "<?php echo $dc1 ?>"><?php echo $disc_four; ?></p>
      <?php endif; ?>
    </div>
  <?php endif; ?>
  </div>
<?php endif; ?>
