<?php

class LP_Recent_Eevent_Wigdet extends WP_Widget {
  function __construct() {
      $widget_ops = array(
      'classname'   => 'widget_recent_entries', 
      'description' => __('Display a list panel of recent and upcoming events.')
    );
      parent::__construct('custom-recent-events', __('Custom List Panel For Events'), $widget_ops);
  }
  function widget($args, $instance) {
      extract( $args );
      $title = apply_filters( 'widget_title', empty($instance['title']) ? 'Recent Posts' : $instance['title'], $instance, $this->id_base);
      //post num
      if ( ! $number = absint( $instance['number'] ) ) $number = 5;
      //catigories
      if( ! $cats = $instance["cats"] )  $cats='';
      //Get the cat names as strings for the ajax load shortcut
      $catNames = '';
      foreach ($cats as $value) {
        $catNames = $catNames . get_term_by('id', $value, 'tribe_events_cat' )->slug .',';
      }
      echo '<div class="list_pannel">';
      echo $before_widget;
      // Widget title
      ?>
        <div class="header">
          <div class="circle"></div>
          <h1>
            <?php echo $instance["title"]; ?>
          </h1>
        </div>
        <div class="line"></div>
        <div class="displayMonth"></div>
      <?php
    // Post list in widget
    echo "<div id='content_wrapper'>\n";
    //event_display="custom" // to display all in current month plus future
    //Ajax shortcode load, I have had to custom talior the alm package to make this work properly
    echo do_shortcode('[ajax_load_more meta_key="_EventStartDate" custom_id="list_pannel-'.$args['widget_id'].'" post_type="tribe_events" taxonomy="tribe_events_cat" orderby="_EventStartDate" taxonomy_terms="'.$catNames.'" event_display="upcoming" repeater="template_4" order="ASC" posts_per_page="' . $number . '" ]');
    echo "</div>\n";
    echo $after_widget;
    echo "</div>\n";
  }
  
  function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['cats'] = isset($new_instance['cats']) ? $new_instance['cats'] : [];
    $instance['number'] = absint($new_instance['number']);
    return $instance;
  }
  
  function form( $instance ) {
    $title = isset($instance['title']) ? esc_attr($instance['title']) : 'Events';
    $number = isset($instance['number']) ? absint($instance['number']) : 5;

    
?>
        <!-- Title -->
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>">
            <strong><?php _e('Title:'); ?></strong>
          </label>
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
         
         <!-- Number of post -->               
        <p>
          <label for="<?php echo $this->get_field_id('number'); ?>">
            <strong><?php _e('Number of posts to show:'); ?></strong>
          </label>
          <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>
        
        <p>
            <!-- Cats -->
            <label for=" . <?php echo $this->id; ?> . ">
              <strong>
                <?php _e('Select categories to include in the recent posts list:');?> 
              </strong>
                <?php
                   $categories =  get_terms("tribe_events_cat");
                     echo "<br/>";
                     foreach ($categories as $cat) {
                          $option='<input type="checkbox" id="'. $this->get_field_id( 'cats' ) .'[]" name="'. $this->get_field_name( 'cats' ) .'[]"';
                          if (isset($instance['cats'])) {
                            if (is_array($instance['cats'])) {
                              foreach ($instance['cats'] as $catsInInstance) {
                                  if($catsInInstance == $cat->term_id) {
                                      $option=$option.' checked="checked"';
                                  }
                              }
                            }
                          }
                          $option .= ' value="'.$cat->term_id.'" />';
                          $option .= '&nbsp;';
                          $option .= $cat->name; 
                          $option .= '<br />';
                          echo $option;
                         }
                    
                    ?>
            </label>
        </p>
<?php
  }
}

function LP_events_register_widget() {
  register_widget( 'LP_Recent_Eevent_Wigdet' );
}

add_action( 'widgets_init', 'LP_events_register_widget' );


?>


