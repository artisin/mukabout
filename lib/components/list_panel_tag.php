<?php
class LP_Recent_Post_Widget_Tag extends WP_Widget {
      
  function __construct() {
      $widget_ops = array(
      'classname'   => 'widget_recent_entries', 
      'description' => __('Display a list of recent post entries from one or more Tags.')
    );
      parent::__construct('recent-posts-by-tags', __('Custom List Panel For Tags'), $widget_ops);
  }

  function widget($args, $instance) {
           
      extract( $args );
    
      $title = apply_filters( 'widget_title', empty($instance['title']) ? 'Recent Posts' : $instance['title'], $instance, $this->id_base);
      $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : true;  
      $orderby_specified = isset( $instance['orderby_specified'] ) ? $instance['orderby_specified'] : false;  

      if ( ! $number = absint( $instance['number'] ) ) $number = 5;
            
      if( ! $tags = $instance["tags"] )  $tags='';
      
      //Get the tag names as strings for the ajax load shortcut
      $tagNames = '';
      foreach ($tags as $value) {
        $tagNames = $tagNames . get_term_by('id', $value, 'post_tag')->slug . ',';
      }

      if ($show_date) {
        echo '<div class="list_pannel">';
      }else{
        echo '<div class="list_pannel no_date">';
      }
      echo $before_widget;
      // Widget title
      ?>
        <div class="header">
          <div class="circle"></div>
          <h1>
            <?php echo $instance["title"]; ?>
          </h1>
        </div>
        <div class="line"></div>
        <?php if ($show_date) : ?>
        <div class="displayMonth"></div>
        <?php endif; ?>
      <?php
    // Post list in widget
    echo "<div id='content_wrapper'>\n";
    //Ajax shortcode load
    if ($orderby_specified) {
      //Orderby Specified
      echo do_shortcode('[ajax_load_more css_classes="listPannel.php" custom_id="list_pannel-'.$args['widget_id'].'" repeater="template_1" orderby="meta_value" meta_key="datePicker" posts_per_page="' . $number . '" tag="'.$tagNames.'"]');
    }elseif (!$show_date) {
      //No date
      echo do_shortcode('[ajax_load_more css_classes="listPannel.php" custom_id="list_pannel-'.$args['widget_id'].' no_date" repeater="template_5" posts_per_page="' . $number . '" tag="'.$tagNames.'" post_type="post" ]');

    }else{
      //default
      echo do_shortcode('[ajax_load_more css_classes="listPannel.php" custom_id="list_pannel-'.$args['widget_id'].'" repeater="template_1" posts_per_page="' . $number . '" tag="'.$tagNames.'" post_type="post" ]');
    }
    echo "</div>\n";
    echo $after_widget;
    echo "</div>\n";

  }
  
  function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['tags'] = isset($new_instance['tags']) ? $new_instance['tags'] : [];
    $instance['number'] = absint($new_instance['number']);
    $instance['show_date'] = isset($new_instance['show_date']) ? (bool) $new_instance['show_date'] : false;
    $instance['orderby_specified'] = isset($new_instance['orderby_specified']) ? (bool) $new_instance['orderby_specified'] : false;
    return $instance;
  }
  
  function form( $instance ) {
    $title = isset($instance['title']) ? esc_attr($instance['title']) : 'Recent Posts';
    $number = isset($instance['number']) ? absint($instance['number']) : 5;
    $show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : true;
    $orderby_specified = isset( $instance['orderby_specified'] ) ? (bool) $instance['orderby_specified'] : false;

    
?>
        <!-- Title -->
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>">
            <strong><?php _e('Title:'); ?></strong>
          </label>
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
         
         <!-- Number of post -->               
        <p>
          <label for="<?php echo $this->get_field_id('number'); ?>">
            <strong><?php _e('Number of posts to show:'); ?></strong>
          </label>
          <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>
        

        <!-- Show Date -->
        <p>
          <input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
          <label for="<?php echo $this->get_field_id( 'show_date' ); ?>">
            <strong><?php _e( 'Display Date?' ); ?></strong>
          </label>
        </p>


        <!-- Order by specified -->
        <p>
          <input class="checkbox" type="checkbox" <?php checked( $orderby_specified ); ?> id="<?php echo $this->get_field_id( 'orderby_specified' ); ?>" name="<?php echo $this->get_field_name( 'orderby_specified' ); ?>" />
          <label for="<?php echo $this->get_field_id( 'orderby_specified' ); ?>">
            <strong><?php _e( 'Order By Specified Date?' ); ?></strong>
          </label>
          <br>
          <i>Note: if there is no date specificied the post will not appear.</i>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('tags'); ?>">
                <strong><?php _e('Select Tags to include in the recent posts list:');?> </strong>
                <?php
                   $tags =  get_tags('hide_empty=0');
                     echo "<br/>";
                     foreach ($tags as $tag) {
                         $option='<input type="checkbox" id="'. $this->get_field_id( 'tags' ) .'[]" name="'. $this->get_field_name( 'tags' ) .'[]"';
                            if (isset($instance['tags'])) {
                              if (is_array($instance['tags'])) {
                                foreach ($instance['tags'] as $tagInstance) {
                                    if($tagInstance == $tag->term_id) {
                                         $option=$option.' checked="checked"';
                                    }
                                }
                              }
                            }
                            $option .= ' value="'.$tag->term_id.'" />';
                            $option .= '&nbsp;';
                            $option .= $tag->name . ' ';
                            $option .= '<br />';
                            echo $option;
                         }
                    
                    ?>
            </label>
        </p>
        
<?php
  }
}

function LP_tag_register_widget() {
  register_widget( 'LP_Recent_Post_Widget_Tag' );
}

add_action( 'widgets_init', 'LP_tag_register_widget' );
?>