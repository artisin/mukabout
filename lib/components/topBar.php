<?php
/**
 * Nav components
 *
 */
include (dirname(__DIR__) . '/include.php');
/**
 * Customize the output of menus for Foundation top bar
 */
class foundation_walker extends Walker_Nav_Menu {
  /**
    * @see Walker_Nav_Menu::start_lvl()
   * @since 1.0.0
   *
   * @param string $output Passed by reference. Used to append additional content.
   * @param int $depth Depth of page. Used for padding.
  */
    function start_lvl( &$output, $depth = 0, $args = array() ) {
      if (isset($args->find_active) && !$args->find_active) {
        $output .= "\n<ul class=\"sub-menu dropdown\">\n";
      }elseif (isset($this->activeTab) && $this->activeTab){
        if ($depth == 0) {
          // PC::debug($this);
          //side nav class parent
          $output .= "\n<ul class=\"side-nav\">\n";
        }else{
          $output .= "\n<ul class=\"side-nav-child\">\n";
        }
      }else{
        return;
      }
    }

    /**
     * @see Walker_Nav_Menu::start_el()
     * @since 1.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item Menu item data object.
     * @param int $depth Depth of menu item. Used for padding.
     * @param object $args
     */

    function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
        $item_html = '';

        parent::start_el( $item_html, $object, $depth, $args );

        $classes = empty( $object->classes ) ? array() : ( array ) $object->classes;  

        //creates parent for sub-side nav
        if (isset($args->activeParentId) && $object->menu_item_parent == $args->activeParentId) {
          if (isset($object->create_sub_lvl) && $object->create_sub_lvl && $object->current_item_parent) {
            $output .= $item_html;
          }
        }

        //For secondary side nav. I have no idea what I doing here
        //there is prbly a much better way to handel this. But the gits
        //is that I want to just get the active nav list to output 
        if (isset($args->find_active) && $args->find_active) {
          //get parrent and set id to corrolate with nav sub-list
          if ($object->current_item_parent || $object->current_item_ancestor) {
            //Parent of subnav list for sublist
            if ($depth == 0 && isset($object->ID) && !isset($args->activeParentId)) {
              $args->activeParentId = $object->ID;
              //for start_lvl so only active gets processed
              $this->activeTab = true;
            }
          }elseif (isset($args->activeParentId)) {
            if (isset($args->home_link_set) && !$args->home_link_set) {
              //adds home link to top of list
              $output .= '<li class="menu-item home-icon"><a href="' . home_url() . '">Home</a>';
              $args->home_link_set = true;
            }
            if ($object->menu_item_parent == $args->activeParentId) {
              $output .= $item_html;
            }elseif ($object->ID == $args->activeParentId) {
              $output .= $item_html;
            }elseif (isset($object->sub_child) && $object->sub_child) {
              //for single nav sub child
              $output .= $item_html;
            }
          }
        }else{
          $output .= $item_html;
        }
    }
    
    function end_lvl( &$output, $depth = 0, $args = array()) {
      //Defualt, not  side-nav
      if (isset($args->find_active) && !$args->find_active) {
        $output .= "</ul>\n";
      }elseif (isset($this->activeTab) && $this->activeTab) {
        $output .= "</ul>\n";
      }else{
        return;
      }
    }
 
  /**
     * @see Walker::display_element()
     * @since 1.0.0
   * 
   * @param object $element Data object
   * @param array $children_elements List of elements to continue traversing.
   * @param int $max_depth Max depth to traverse.
   * @param int $depth Depth of current element.
   * @param array $args
   * @param string $output Passed by reference. Used to append additional content.
   * @return null Null on failure with no changes to parameters.
   */
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
        //has children
        $element->has_children = !empty( $children_elements[$element->ID] );
        
        //if depth is grater than one with children - for side nav
        if (isset($children_elements[$element->ID]) && $children_elements[$element->ID] && $depth !== 0) {
          $find_active = array_filter($args, function($v) { return $v->find_active; });
          if ($find_active) {
            $element->create_sub_lvl = true;
            $element->classes[] = ( $element->current || $element->current_item_ancestor ) ? 'active' : '';
            $element->classes[] = ( $element->has_children ) ? 'single-with-children' : 'single-no-children';
            $ID = $element->ID;
            //find sub-children
            foreach ($children_elements as $val) {
              foreach ($val as $indv) {
                if ($indv->menu_item_parent == $ID) {
                  $indv->sub_child = true;
                }
              }
            }
          }
          parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
        }else{
          $element->has_children = !empty( $children_elements[$element->ID] );
          $element->classes[] = ( $element->current || $element->current_item_ancestor ) ? 'active' : '';
          $element->classes[] = ( $element->has_children ) ? 'has-dropdown top-level-tab' : '';
          //for js hover config
          $element->classes[] = 'sub-menu-id-' .$element->menu_item_parent;
          if ($element->menu_item_parent === "0") {
            // $element
            $element->classes[] = 'navTab';
            $element->classes[] = $element->post_name;

          };
          parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
        }


    }

}


/**
 * Top bar
 * http://codex.wordpress.org/Function_Reference/wp_nav_menu
 */
// function foundation_top_bar() {
//     wp_nav_menu(array( 
//         'container' => false,                           // remove nav container
//         'container_class' => '',              // class of container
//         'menu' => '',                               // menu name
//         'menu_class' => 'top-bar-menu right',          // adding custom nav class
//         'theme_location' => 'header',                // where it's located in the theme
//         'before' => '',                                 // before each link <a> 
//         'after' => '',                                  // after each link </a>
//         'link_before' => '',                            // before each link text
//         'link_after' => '',                             // after each link text
//         'depth' => 5,                                   // limit the depth of the nav
//         'fallback_cb' => false,                         // fallback function (see below)
//         'walker' => new foundation_walker()
//   ));
// }


/**
 * Left top bar
 */
function foundation_top_bar() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'find_active' => false,
        'home_link_set' => false,
        'container_class' => '',              // class of container
        'menu' => '',                               // menu name
        'menu_class' => '',          // adding custom nav class
        'theme_location' => 'header',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 2,
        'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new foundation_walker()
  ));
}
function foundation_top_bar_active() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'find_active' => true,
        'home_link_set' => false,
        'container_class' => '',              // class of container
        'menu' => '',                               // menu name
        'menu_class' => '',          // adding custom nav class
        'theme_location' => 'header',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
        'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new foundation_walker()
  ));
}
/**
 * Right top bar
 */
function foundation_top_bar_r() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'container_class' => '',              // class of container
        'menu' => '',                               // menu name
        'menu_class' => 'top-bar-menu right',           // adding custom nav class
        'theme_location' => 'top-bar-r',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
      'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new foundation_walker()
  ));
}


/**
 * Custom Walker for Foundation Bottom Nav
 */
function foundation_footerNav() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'container_class' => '',              // class of container
        'menu' => '',                               // menu name
        'menu_class' => 'inline-list',                   // adding custom nav class
        'theme_location' => 'footer',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
        'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new foundation_walker()
  ));
}
