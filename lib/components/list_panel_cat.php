<?php

class Custom_Recent_Posts_Widget extends WP_Widget {
	function __construct() {
    	$widget_ops = array(
			'classname'   => 'widget_recent_entries', 
			'description' => __('Display a list of recent post entries from one or more categories.')
		);
    	parent::__construct('custom-recent-posts', __('Custom List Panel For Categories'), $widget_ops);
	}
  function widget($args, $instance) {
			extract( $args );
			$title = apply_filters( 'widget_title', empty($instance['title']) ? 'Recent Posts' : $instance['title'], $instance, $this->id_base);
      $orderby_specified = isset( $instance['orderby_specified'] ) ? $instance['orderby_specified'] : false;  
      $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : true;  

      //post num
			if ( ! $number = absint( $instance['number'] ) ) $number = 5;
      //catigories
			if( ! $cats = $instance["cats"] )  $cats='';

      //Get the cat names as strings for the ajax load shortcut
      $catNames = '';
      foreach ($cats as $value) {
        $catNames = $catNames . get_the_category_by_ID( $value ) . ',';
      }

      if ($show_date) {
        echo '<div class="list_pannel">';
      }else{
        echo '<div class="list_pannel no_date">';
      }
      echo $before_widget;
      // Widget title
      ?>
        <div class="header">
          <div class="circle"></div>
          <h1>
            <?php echo $instance["title"]; ?>
          </h1>
        </div>
        <div class="line"></div>
        <?php if ($show_date) : ?>
        <div class="displayMonth"></div>
        <?php endif; ?>
      <?php
		// Post list in widget
		echo "<div id='content_wrapper'>\n";
		//Ajax shortcode load
    if ($orderby_specified) {
      //Orderby Specified
      echo do_shortcode('[ajax_load_more css_classes="listPannel.php" custom_id="list_pannel-'.$args['widget_id'].'" repeater="template_1" orderby="meta_value" meta_key="datePicker" posts_per_page="' . $number . '" category="'.$catNames.'"]');
    }elseif (!$show_date) {
      //no date
      echo do_shortcode('[ajax_load_more css_classes="listPannel.php" custom_id="list_pannel-'.$args['widget_id'].' no_date" repeater="template_5"  posts_per_page="' . $number . '" category="'.$catNames.'"]');
    }else{
      //default
      echo do_shortcode('[ajax_load_more css_classes="listPannel.php" custom_id="list_pannel-'.$args['widget_id'].'" repeater="template_1" posts_per_page="' . $number . '" category="'.$catNames.'"]');
    }
		echo "</div>\n";
    echo $after_widget;
    echo "</div>\n";
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
    $instance['cats'] = isset($new_instance['cats']) ? $new_instance['cats'] : [];
		$instance['number'] = absint($new_instance['number']);
    $instance['show_date'] = isset($new_instance['show_date']) ? (bool) $new_instance['show_date'] : false;
    $instance['orderby_specified'] = isset($new_instance['orderby_specified']) ?  (bool) $new_instance['orderby_specified'] : false;
    return $instance;
	}
	
	function form( $instance ) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : 'Recent Posts';
		$number = isset($instance['number']) ? absint($instance['number']) : 5;
    $orderby_specified = isset( $instance['orderby_specified'] ) ? (bool) $instance['orderby_specified'] : false;
    $show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : true;

		
?>  
        <!-- Title -->
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>">
            <strong><?php _e('Title:'); ?></strong>
          </label>
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
                        
         <!-- Number of post -->               
        <p>
          <label for="<?php echo $this->get_field_id('number'); ?>">
            <strong><?php _e('Number of posts to show:'); ?></strong>
          </label>
          <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>
          
        <!-- Show Date -->
        <p>
          <input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
          <label for="<?php echo $this->get_field_id( 'show_date' ); ?>">
            <strong><?php _e( 'Display Date?' ); ?></strong>
          </label>
        </p>


        <!-- Order by specified -->
        <p>
          <input class="checkbox" type="checkbox" <?php checked( $orderby_specified ); ?> id="<?php echo $this->get_field_id( 'orderby_specified' ); ?>" name="<?php echo $this->get_field_name( 'orderby_specified' ); ?>" />
          <label for="<?php echo $this->get_field_id( 'orderby_specified' ); ?>">
            <strong><?php _e( 'Order By Specified Date?' ); ?></strong>
          </label>
          <br>
          <i>Note: if there is no date specificied the post will not appear.</i>
        </p>

        <!-- Cats -->
        <p>
            <label for="<?php echo $this->get_field_id('cats'); ?>">
              <strong>
                <?php _e('Select categories to include in the recent posts list:');?>
              </strong> 
            
                <?php
                   $categories=  get_categories('hide_empty=0');
                     echo "<br/>";
                     foreach ($categories as $cat) {
                         $option='<input type="checkbox" id="'. $this->get_field_id( 'cats' ) .'[]" name="'. $this->get_field_name( 'cats' ) .'[]"';
                            if (isset($instance['cats'])) {
                              if (is_array($instance['cats'])) {
                                  foreach ($instance['cats'] as $catsInInstance) {
                                      if($catsInInstance == $cat->term_id) {
                                           $option=$option.' checked="checked"';
                                      }
                                  }
                              }
                            }
                            $option .= ' value="'.$cat->term_id.'" />';
			                      $option .= '&nbsp;';
                            $option .= $cat->cat_name;
                            $option .= '<br />';
                            echo $option;
                         }
                    
                    ?>
            </label>
        </p>
        
<?php
	}
}

function crpw_register_widgets() {
	register_widget( 'Custom_Recent_Posts_Widget' );
}

add_action( 'widgets_init', 'crpw_register_widgets' );


?>


