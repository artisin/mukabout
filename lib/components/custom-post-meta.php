<?php

//Add to Calander
function scpMetaAddToCalander()
{
  if ( ! class_exists( 'Super_Custom_Post_Type' ) )
    return;
  $meta = new Super_Custom_Post_Meta( 'post' );

  $meta->add_meta_box( array(
      'id' => 'add_To_Calander_sidebar',
      'context' => 'side',
      'priority' => 'high',
      'title' => 'Calander / Specify A date',
      'fields' => array(
          'addToCalander'=> array( 
            'type' => 'boolean',
            'label' => 'Add it to the Calander! (Note: if you do not specify a date below it will defualt to the post date)' 
          ),
          'datePicker'=> array( 
          'type' => 'date',
          'label' => 'Date Picker (For List Panel and Calander)',
          ),
          'startTime' => array(
            'type' => 'time',
            'label' => 'Start Time' 
          ),
          'endTime' => array(
            'type' => 'time',
            'label' => 'End Time' 
          ),
      )
  ) );
  //add a to coloum
  $meta->add_to_columns( array('addToCalander' => 'On Calander' ) );
}

add_action( 'after_setup_theme', 'scpMetaAddToCalander' );




//Contact side bar
function scpMetaContact()
{
  if ( ! class_exists( 'Super_Custom_Post_Type' ) )
    return;
  $meta = new Super_Custom_Post_Meta( 'page' );

  $meta->add_meta_box( array(
      'id' => 'contact_sidebar',
      'context' => 'normal',
      'fields' => array(
        'Enable Contact Sidebar'=> array( 'type' => 'boolean' ),
        'info'=> array( 'type' => 'wysiwyg' ),
      )
  ) );
  
}

add_action( 'after_setup_theme', 'scpMetaContact' );