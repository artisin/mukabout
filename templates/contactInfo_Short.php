<div class="contactInfo_short">
  <!-- Telly -->
  <div class="telephone">
    <div class="contact_label">
      <span class="contact_data">
        <?php 
          $contact_telephone = get_theme_mod( 'contact_telephone', '' ); 
          if ( $contact_telephone ) {
            $cleanNum = filter_var($contact_telephone, FILTER_SANITIZE_NUMBER_INT);
            echo "<a href=tel:'".$cleanNum."'>".$contact_telephone."</a>";
          } else {
            echo '<p>You need to add a telephone!</p>';
          }
        ?> 
      </span>
    </div>
  </div>
  <!-- Email -->
  <div class="email">
    <div class="contact_label">
      <span class="contact_data">
        <?php 
          $contact_email = get_theme_mod( 'contact_email', '' ); 
          if ( $contact_email ) {
            echo "<a href='mailto:".$contact_email."'>".$contact_email."</a>";
          } else {
            echo '<p>You need to add a Email!</p>';
          }
        ?> 
      </span>
    </div>
  </div>
  <!-- Address  -->
  <div class="address">
    <div class="contact_label">
      <span class="contact_data">
        <?php 
          $contact_address = get_theme_mod( 'contact_address', '' ); 
          if ( $contact_address ) {
            echo ( "<a href='https://maps.google.com?q=625+South+Rochester+Street+Mukwonago+WI+53149'>".$contact_address."</a>" );
          } else {
            echo '<p>You need to add a address!</p>';
          }
        ?> 
      </span>
    </div>
  </div>
</div>

