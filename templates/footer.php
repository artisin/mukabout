<footer id="footer" role="contentinfo">
  <!-- Footer -->
  <div class="footer_widget_wrap">
    <!-- Social Media Icons -->
    <div class="footer_widget footer_left">
      <div class="footer_title">
        <h5>Follow Us</h5>
      </div>
      <ul class = "social_icons">
        <?php get_template_part('templates/socialMediaIcons'); ?>
      </ul>
    </div>

    <!-- Mailing List -->
    <div class="footer_widget footer_middle">
      <?php if ( is_active_sidebar( 'footer_sidebar-2' ) ) : ?> 
      <div class="footer_title">
        <h5>Keep Up To Date</h5>
      </div>
      <?php { dynamic_sidebar( 'footer_sidebar-2' ); } ?>
      <?php endif; ?>
    </div>

    <!-- Contact Details -->
    <div class="footer_widget footer_right">
      <div class="footer_title">
        <h5>Get In Touch</h5>
      </div>
      <?php get_template_part('templates/contactInfo'); ?>
    </div>
  </div>
  <div class="shadow_widget_footer"></div>

<!-- Twitter Feed -->
<div class="twitterFeed_footer_Wrapper">
  <div class="twitterFeed_footer">
    <?php if ( is_active_sidebar( 'footer_sidebar-twitter' ) ) : ?> 
      <!-- Twitter icon -->
      <div class="twitterIcon">
        <a class="twitter-icon" target="_blank" rel="nofollow" href="https://twitter.com/">
          <i class="fa fa-twitter"></i>
        </a>
        <!-- Handled by js -->
        <div class="tweetCount_Wrapper">
          <div class="tweetCount">
            <span class="currentTweet"></span>
            <span></span>
          </div>
        </div>
      </div>
    <?php { dynamic_sidebar( 'footer_sidebar-twitter' ); } ?>
    <!-- Tweet Changer -->
    <div class="tweetChanger">
      <div class="tweet_Prev tweet_button">
        <i class="fa fa-arrow-up"></i>
      </div>
      <div class="tweet_Next tweet_button">
        <i class="fa fa-arrow-down"></i>
      </div>
    </div>
    <?php endif; ?>
  </div>
</div>
<!-- <div class="shadow_widget_footer"></div> -->
<!-- Google Maps -->
<div class="googleMaps_footer">
  <?php $google_maps = get_theme_mod( 'google_maps', '' ); if ( $google_maps ) : ?>
    <div id="map-canvas"></div>
  <?php endif; ?>
</div>



  <?php if (has_nav_menu('footer')) : ?> 
    <div class="footer_nav">
      <?php
        foundation_footerNav();
      ?>
    </div>
  <?php endif; ?>
  

</footer>
