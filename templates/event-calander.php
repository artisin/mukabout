<header>
  <h1 class="event_Title">
    <?php tribe_events_title ( true ); ?>
  </h1>
</header>
<?php while (have_posts()) : the_post(); ?>
  <div class="event_Container">
    <article>
      <?php the_content(); ?>
    </article>
  </div>

<?php endwhile; ?>