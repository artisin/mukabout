<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Merriweather:400,400italic,700italic,700' rel='stylesheet' type='text/css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//vjs.zencdn.net/4.12/video.js"></script>
  <script src="/wp-content/themes/mukabout/dist/js/youtube.js"></script>
  <!-- Ref link for js -->
  <script type="text/javascript">
    var homeUrl = '<?= home_url() ?>';
  </script>
  <?php wp_head(); ?>
</head>

