<div class="contact_info">
  <!-- Address  -->
  <div class="address">
    <p class="contact_label">Address</p>
    <div class="contact_data">
      <?php 
        $contact_address = get_theme_mod( 'contact_address', '' ); 
        if ( $contact_address ) {
          echo ( "<a href='https://maps.google.com?q=625+South+Rochester+Street+Mukwonago+WI+53149'>".$contact_address."</a>" );
        } else {
          echo '<p>You need to add a address!</p>';
        }
      ?> 
    </div>
  </div>
  <!-- Telly -->
  <div class="telephone">
    <p class="contact_label">Telephone</p>
    <div class="contact_data">
      <?php 
        $contact_telephone = get_theme_mod( 'contact_telephone', '' ); 
        if ( $contact_telephone ) {
          $cleanNum = filter_var($contact_telephone, FILTER_SANITIZE_NUMBER_INT);
          echo "<a href=tel:'".$cleanNum."'>".$contact_telephone."</a>";
        } else {
          echo '<p>You need to add a telephone!</p>';
        }
      ?> 
    </div>
  </div>
  <!-- Email -->
  <div class="email">
    <p class="contact_label">Email</p>
    <div class="contact_data">
      <?php 
        $contact_email = get_theme_mod( 'contact_email', '' ); 
        if ( $contact_email ) {
          echo "<a href='mailto:".$contact_email."'>".$contact_email."</a>";
        } else {
          echo '<p>You need to add a Email!</p>';
        }
      ?> 
    </div>
  </div>
</div>

