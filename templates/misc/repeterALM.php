<!-- Non-Specified date -->
<div class="object">
  <div class="info">
    <div class="date">
      <div>
        <?php echo get_the_date('d'); ?>
      </div>
      <div class="month">
        <div class="monthText">
          <?php  echo get_the_date('M'); ?>
        </div>
        <div class="monthLine"></div>
      </div>
    </div>
    <div class="circle"></div>
  </div>
  <div class="context">
    <div class="title">
      <span>
        <?php the_title(); ?>
      </span>
    </div>
    <div class="content">
      <?php the_excerpt(); ?>
      
    <a href = "<?php the_permalink(); ?>" class="linkToArticle">
      <i class="fa fa-link"></i>
    </a>
    </div>
  </div>
</div>


<!-- Specified date -->
<div class="object">
  <div class="info">
    <div class="date">
      <div>
        <?php 
          global $post; 
          $customDate = get_post_meta($post->ID, 'datePicker', true);
          echo date('d', strtotime($customDate));
        ?>
      </div>
      <div class="month">
        <div class="monthText">
          <?php
            global $post; 
            $customDate = get_post_meta($post->ID, 'datePicker', true);
            echo date('M', strtotime($customDate));
          ?>
        </div>
        <div class="monthLine"></div>
      </div>
    </div>
    <div class="circle"></div>
  </div>
  <div class="context">
    <div class="title">
      <span>
        <?php the_title(); ?>
      </span>
    </div>
    <div class="content">
      <?php the_excerpt(); ?>
      
    <a href = "<?php the_permalink(); ?>" class="linkToArticle">
      <i class="fa fa-link"></i>
    </a>
    </div>
  </div>
</div>


<!-- Event Calander -->
<div class="object">
  <div class="info">
    <div class="date">
      <div>
        <?php 
          global $post; 
          echo date('d', strtotime($post->EventStartDate));
        ?>
      </div>
      <div class="month">
        <div class="monthText">
          <?php
            global $post; 
            echo date('M', strtotime($post->EventStartDate));
          ?>
        </div>
        <div class="monthLine"></div>
      </div>
    </div>
    <div class="circle"></div>
  </div>
  <div class="context">
    <div class="title">
      <span>
        <?php the_title(); ?>
      </span>
    </div>
    <div class="content">
      <?php the_excerpt(); ?>
      
    <a href = "<?php the_permalink(); ?>" class="linkToArticle">
      <i class="fa fa-link"></i>
    </a>
    </div>
  </div>
</div>

//Query need for events
// $q_args = array(
//   'post_status'=>'publish',
//   'post_type'=>array(TribeEvents::POSTTYPE),
//   'posts_per_page'=>10,
//   //order by startdate from newest to oldest
//   // 'meta_key'=>'_EventStartDate',
//   'orderby'=>'_EventStartDate',
//   'order'=>'DESC',
//   //required in 3.x
//   'eventDisplay'=>'custom',
//   //query events by category
//   'tax_query' => array(
//       array(
//           'taxonomy' => 'tribe_events_cat',
//           'field' => 'slug',
//           'terms' => 'community-events',
//           'operator' => 'IN'
//       ),
//   )
// );
// // PC::debug($q_args);
// $get_posts = null;
// $get_posts = new WP_Query($q_args);
// if($get_posts->have_posts()) : while($get_posts->have_posts()) : $get_posts->the_post();
//   global $post;
//   PC::debug($post);
//   PC::debug($post->EventStartDate);
//   PC::debug(date('M', strtotime($post->EventStartDate)));
//   endwhile;
//   endif;
// wp_reset_postdata();



<!-- No date -->
<div class="object">
  <div class="info">
    <div class="circle"></div>
  </div>
  <div class="context">
    <div class="title">
      <span>
        <?php the_title(); ?>
      </span>
    </div>
    <div class="content">
      <?php the_excerpt(); ?>
    <a href = "<?php the_permalink(); ?>" class="linkToArticle">
      <i class="fa fa-link"></i>
    </a>
    </div>
  </div>
</div>