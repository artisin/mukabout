<header class="banner" role="banner">
  <!-- Ajax -->
  <?php if (has_nav_menu('header')) : ?>
    <!-- Sidebar nav -->
    <div class="top-bar-container fixed">
      <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
          <li class="name show-for-small">
            <h1><a href="#">Mukwonago</a></h1>
          </li>
           <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
          <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>
          <section class="top-bar-section" id="primaryNav">
            <?php
              foundation_top_bar();
            ?>
          </section>
      </nav>
      <div class="menuDropShadow"></div>
    </div>
  <?php endif; ?>
  <!-- Sub Header -->
  <div class="sub-header_Wrapper">
    <div class="sub-header_Container">
      <div class="logo_Container">
        <div class="logo">
          <!-- <img src="../wp-c  ontent/themes/mukabout/dist/images/village-of-mukwonago.fw_.png" alt=""> -->
          <div class="top">
            The Village of
          </div>
          <div class="middle">
            <span class='start'>Mukw</span>
            <span class='bearClaw'>o</span>
            <span class='end'>nago</span>
          </div>
          <div class="bottom">
            Where life, leisure and business thrive.
          </div>
        </div>
      </div>
      <div class="contactUs">
        <?php get_template_part('templates/contactInfo_Short'); ?>
      </div>
    </div>
  </div>
  <div class="sub-header_btm-shadow"></div>
</header>

