<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
      <!--[if lt IE 8]>
        <div class="alert alert-warning">
          <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'mukabout'); ?>
        </div>
      <![endif]-->
      <?php
        do_action('get_header');
        get_template_part('templates/header');
      ?>
      <?php if (is_home()) : ?>
        <div class="home_Wrapper" role="document">
      <!-- Calander Grid -->
      <?php elseif (tribe_is_month() || tribe_is_event_query()): ?>
        <div class="event_Wrapper" role="document">
      <!-- Single -->
      <?php else: ?>
        <!-- Single side nav -->
        <div class="subPage_Wrapper" role="document">
        <div id="sideNav_Wrapper">
          <?php
            foundation_top_bar_active();
          ?>
          <div class="auxSingleMenu"></div>
        </div>
      <?php endif; ?>
        
        <?php if (!tribe_is_event_query()) : ?>
        <main class="main content_Wrapper" role="main" id="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php else : get_template_part('templates/event-calander');?>
        <?php endif; ?>

        <?php if (Config\display_sidebar() && !tribe_is_event_query()) : ?>
          <aside class="sidebar" role="complementary">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.wrap -->
      <?php
        do_action('get_footer');
        get_template_part('templates/footer');
      ?>
      <!-- Google maps -->
      <?php $google_maps = get_theme_mod( 'google_maps', '' ); if ( $google_maps ) : ?>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACA6WcwJP5aLiCR-a-gkCbUMRe69PmHuc&sensor=false"></script>
      <?php endif; ?>
      <!-- Footer Scripts -->
      <?php
        wp_footer();
      ?>
  </body>
</html>
